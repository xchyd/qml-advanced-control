﻿import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import QtQuick.Controls 2.12 as Control2   //同时使用control1.4和control2.x时必须要重命名，否则QtCreator会识别冲突
import QtQuick.Controls 1.4 as Control1
import QtQuick.Controls.Styles 1.4
import "../style.js" as Style

/*
    日期控件
*/
Control2.Menu{
    id:controlRoot

    //日期数据
    property alias  selectedDate : calendar.selectedDate              //设置当前选中的日期：一个Date对象
    property alias  maximumDate : calendar.maximumDate                //设置最大可选日期：一个Date对象
    property alias  minmumDate : calendar.minimumDate                 //设置最小可选日期：一个Date对象
    readonly property int  selectedYear : selectedDate.getFullYear()  //获取当前选择的年份，例如：2020
    readonly property int  selectedMonth : selectedDate.getMonth()+1  //获取当前选择的月份，例如：10
    readonly property int  selectedDay : selectedDate.getDate()       //获取当前选择的天数，例如：1

    //导航栏样式
    property int    navigationBarHeight:Style.size_titleText*2
    property color  navigationBarBackColor:Style.color_flatBlue
    property color  navigationBarDividerColor:"white"
    property int    navigationBarTitleTextSize:Style.size_titleText
    property color  navigationBarTitleTextColor:"white"
    property color  navigationBarButtonColor:"white"

    //周样式
    property int    weekItemHeight:Style.size_bodyText*2
    property color  weekItemBackColor:navigationBarBackColor
    property color  weekItemTextColor:navigationBarTitleTextColor
    property int    weekItemTextSize:Style.size_bodyText

    //日样式
    property color  dayTextInValidColor:Style.color_textLightgray
    property color  dayTextSelectedColor:"white"
    property color  dayTextNormalColor:Style.color_textBlack
    property int    dayTextSize:Style.size_bodyText *0.8
    property color  dayBgInValidColor:dayBgNormalColor
    property color  dayBgSelectedColor:Style.color_flatBlue
    property color  dayBgNormalColor:"white"

    //其他样式
    property bool   showShadow:true                   //是否显示阴影
    property color  gridColor:Style.color_borderGray  //内框线颜色
    property bool   enableAnimation:true              //显示动画效果

    //信号
    signal clicked(var date)    //鼠标点选某个日期项目时触发
    signal hovered(var date)    //鼠标悬浮在某个日期项时触发

    width: 300
    height: 300
    padding: 0
    spacing: 0

    //背景颜色
    background: Rectangle{
        color: "transparent"
    }

    //显示动画
    enter: Transition {
        enabled: enableAnimation
        NumberAnimation {
            property: "y";
            from: controlRoot.y+20
            to:controlRoot.y
            duration: 250
        }
    }

    //日历
    Rectangle{
        id:frame
        width:controlRoot.width-3
        height: controlRoot.height-5

        Control1.Calendar{
            id:calendar
            width:parent.width
            height: parent.height
            frameVisible: true
            style: CalendarStyle{
                gridColor: gridColor
                navigationBar: navigationBarComp
                dayOfWeekDelegate: weekComp
                dayDelegate : dayComp
            }

            MouseArea{   //实现滚轮切换显示的月份
                anchors.fill: parent
                acceptedButtons:Qt.MiddleButton
                onWheel: {
                    if(wheel.angleDelta.y>0){ //向上滚动
                        calendar.showPreviousMonth()
                    }else{
                        calendar.showNextMonth()
                    }
                }
            }

            onClicked: {
                controlRoot.close()
                controlRoot.clicked(date)
            }
            onHovered: {
                controlRoot.hovered(date)
            }
        }
    }

    //边框阴影
    DropShadow {
        cached: true
        horizontalOffset: 1
        verticalOffset: 3
        samples:17
        radius: 8
        smooth: true
        color: gridColor
        anchors.fill: frame
        source: frame
        visible: controlRoot.showShadow
    }

    //导航栏组件
    Component{
        id:navigationBarComp

        Rectangle{
            width:controlRoot.width
            height: navigationBarHeight
            color: navigationBarBackColor

            RowLayout{
                width: parent.width
                height: parent.height

                Item {
                    Layout.fillHeight: true
                    Layout.minimumWidth: 5
                    Layout.maximumWidth: 5
                }

                Control2.Button{  //上一年
                    id:yearButton1
                    hoverEnabled: true
                    Layout.fillHeight: true
                    Layout.minimumWidth: navigationBarTitleTextSize*2
                    Layout.maximumWidth: navigationBarTitleTextSize*2
                    background: Rectangle{
                        border.width: 0
                        color: "transparent"
                    }
                    text: "<<"
                    palette.buttonText:navigationBarTitleTextColor
                    font.pixelSize: navigationBarTitleTextSize
                    onPressed: opacity=0.3
                    onReleased: opacity=1
                    onClicked: calendar.showPreviousYear()
                }

                Control2.Button{  //上一月
                    id:monthButton1
                    hoverEnabled: true
                    Layout.fillHeight: true
                    Layout.minimumWidth: navigationBarTitleTextSize
                    Layout.maximumWidth: navigationBarTitleTextSize
                    background: Rectangle{
                        border.width: 0
                        color: "transparent"
                    }
                    text: "<"
                    palette.buttonText:navigationBarTitleTextColor
                    font.pixelSize: navigationBarTitleTextSize
                    onPressed: opacity=0.3
                    onReleased: opacity=1
                    onClicked: calendar.showPreviousMonth()
                }

                Text {
                    id: yearText
                    text: __formatCalendarTitle(styleData.title)
                    color: navigationBarTitleTextColor
                    font.pixelSize: navigationBarTitleTextSize
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }

                Control2.Button{ //下一月
                    id:monthButton2
                    hoverEnabled: true
                    Layout.fillHeight: true
                    Layout.minimumWidth: navigationBarTitleTextSize
                    Layout.maximumWidth:navigationBarTitleTextSize
                    background: Rectangle{
                        border.width: 0
                        color: "transparent"
                    }
                    text: ">"
                    palette.buttonText:navigationBarTitleTextColor
                    font.pixelSize: navigationBarTitleTextSize
                    onPressed: opacity=0.3
                    onReleased: opacity=1
                    onClicked: calendar.showNextMonth()
                }

                Control2.Button{  //下一年
                    id:yearButton2
                    hoverEnabled: true
                    Layout.fillHeight: true
                    Layout.minimumWidth: navigationBarTitleTextSize*2
                    Layout.maximumWidth: navigationBarTitleTextSize*2
                    background: Rectangle{
                        border.width: 0
                        color: "transparent"
                    }
                    text: ">>"
                    palette.buttonText:navigationBarTitleTextColor
                    font.pixelSize: navigationBarTitleTextSize
                    onPressed: opacity=0.3
                    onReleased: opacity=1
                    onClicked: calendar.showNextYear()
                }

                Item {
                    Layout.fillHeight: true
                    Layout.minimumWidth: 5
                    Layout.maximumWidth: 5
                }
            }

            Rectangle{
                width: parent.width
                height: 1
                color:navigationBarDividerColor
                anchors.bottom: parent.bottom
                opacity: 0.3
            }
        }
    }

    //周项组件
    Component{
        id:weekComp
        Rectangle{
            height:weekItemHeight
            color: weekItemBackColor
            Text {
                anchors.fill: parent
                text: __formatCalendarWeek(styleData.index)
                color: weekItemTextColor
                font.pixelSize:weekItemTextSize
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
            Rectangle{
                width: 1
                height: parent.height
                color: gridColor
                anchors.right: parent.left
            }
        }
    }

    //日项组件
    Component{
        id:dayComp
        Rectangle {
            color: styleData.valid?(styleData.visibleMonth?(styleData.selected?dayBgSelectedColor:dayBgNormalColor):dayBgInValidColor):dayBgInValidColor
            border.width: 2
            border.color: styleData.hovered?dayBgSelectedColor:"transparent"

            Control2.Label {
                text: styleData.date.getDate()
                font.pixelSize: dayTextSize
                anchors.centerIn: parent
                color: styleData.valid?(styleData.visibleMonth?(styleData.selected?dayTextSelectedColor:dayTextNormalColor):dayTextInValidColor):dayTextInValidColor
            }
        }
    }

    //格式化导航栏标题文字
    function __formatCalendarTitle(title){
        var month = title.split(" ")[0]
        var year = title.split(" ")[1]

        if(month==="一月"){
            month="1月"
        }else if(month==="二月"){
            month="2月"
        }else if(month==="三月"){
            month="3月"
        }else if(month==="四月"){
            month="4月"
        }else if(month==="五月"){
            month="5月"
        }else if(month==="六月"){
            month="6月"
        }else if(month==="七月"){
            month="7月"
        }else if(month==="八月"){
            month="8月"
        }else if(month==="九月"){
            month="9月"
        }else if(month==="十月"){
            month="10月"
        }else if(month==="十一月"){
            month="11月"
        }else if(month==="十二月"){
            month="12月"
        }

        return year+"年  "+month
    }

    //格式化周栏文字
    function __formatCalendarWeek(week){
        if(week===0){
            return "一"
        }else if(week===1){
            return "二"
        }else if(week===2){
            return "三"
        }else if(week===3){
            return "四"
        }else if(week===4){
            return "五"
        }else if(week===5){
            return "六"
        }else if(week===6){
            return "日"
        }

        return ""
    }

}
