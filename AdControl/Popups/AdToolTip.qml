﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtGraphicalEffects 1.14
import "../style.js" as Style

/*
    工具提示控件
*/
ToolTip{
    id: controlRoot

    property color  textColor:Style.color_textGray              //字体颜色
    property int    textSize:Style.size_bodyText                  //字体大小
    property string textFamily:Style.string_textFamily            //字体族

    property color  bgColor:"white"                  //背景颜色
    property color  bgBorderColor: Style.color_borderGray       //边框线颜色
    property int    bgBorderWidth:1                  //边框线宽度
    property int    bgBorderRadius:0                 //边框线宽度

    delay: 200
    timeout: 0
    clip:true

    contentItem: Text {
        clip: true
        text: controlRoot.text
        font.pixelSize: textSize
        font.family: textFamily
        wrapMode:Text.NoWrap
        color: textColor
    }

    background: Rectangle {
        border.width: 1
        border.color: bgBorderColor
        color: bgColor
        radius: bgBorderRadius
    }

    Timer{
        id:delayTimer
        interval:controlRoot.delay<0?0:controlRoot.delay
        repeat: false
        onTriggered:{
            __show()
            if(controlRoot.timeout>0){
                timeOutTimer.restart()
            }
        }
    }

    Timer{
        id:timeOutTimer
        interval: controlRoot.timeout
        repeat: false
        onTriggered:{
             __close()
        }
    }

    MouseArea{
        id:mouseArea
        parent:controlRoot.parent
        anchors.fill: parent
        acceptedButtons:Qt.NoButton   //避免拦截父控件鼠标事件
        hoverEnabled: true
        onEntered: {            
            delayTimer.restart();
        }
        onExited: {
            __close()
        }
    }

    function __show(){
        controlRoot.x = mouseArea.mouseX+15
        controlRoot.y =  mouseArea.mouseY+15
        controlRoot.open()
    }

    function __close(){
        controlRoot.close()
    }
}
