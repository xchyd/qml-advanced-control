﻿import QtQuick.Controls 2.12
import QtQuick 2.12
import "../style.js" as Style

/*
    滚动文本控件
*/
Rectangle {
    id:controlRoot

    property string text: ""                              //滚动的文本
    property int    textSize: Style.size_bodyText         //字体大小
    property color  textColor: "white"                    //字体颜色
    property string textFamily:Style.string_textFamily    //字体族
    property color  bgColor: Style.color_flatBlue         //背景颜色
    property int    rollingSpeed: 4                       //每秒滚动字符数

    implicitWidth: 300
    implicitHeight: textSize*2
    clip: true
    color:bgColor

    Text{
        id:contentText
        text:controlRoot.text
        color: textColor
        font.pixelSize: textSize
        font.family:  textFamily
        height: parent.height
        x:controlRoot.width+1
        verticalAlignment: Text.AlignVCenter
        wrapMode:Text.NoWrap
        clip: false

        //文本宽度改变时重新启动动画。不应在text改变时去重启动画，因为它
        //触发时contentText.contentWidth可能还没来得及改变
        onContentWidthChanged: {           
            if(contentText.text===""){
                animation.running = false
            }else{
                animation.from = controlRoot.width+1
                animation.to = -contentText.contentWidth
                animation.duration = (controlRoot.width+contentText.contentWidth)/(rollingSpeed*textSize)*1000
                animation.restart()
            }
        }
    }

    PropertyAnimation{
        id:animation
        target:contentText
        property: "x"
        loops:Animation.Infinite
    }
}
