## Qml Advanced Controls

**欢迎邮件交流技术问题：1552883346@qq.com**

<img src="https://svg.hamm.cn/badge.svg?key=lang&value=Qml&color=44C07D"/>
<img src="https://svg.hamm.cn/badge.svg?key=version&value=Qt5.14&color=1182C2"/>
<img src="https://svg.hamm.cn/badge.svg?key=controls&value=30%2B&color=EC4758"/>
<img src="https://svg.hamm.cn/badge.svg?key=license&value=LGPL-3.0&color=F38D30"/>

### 1 介绍
本项目实现了一个基于前端风格的Qml控件模块，其具有以下特性：
* 完全响应式控件，自动根据model等属性动态渲染。
* 扁平化的外观样式，细粒度的属性设置。
* 基于最新的 QtQuick Canvas 以及 QtQuick Control2 技术实现。
* 所有控件均使用单个.qml文件实现，既可以采用模块方式调用，也可以单独提取出来使用。


### 2 使用
（1）复制AdControl文件夹到项目目录下，并将整个文件夹添加到qrc资源文件中。  

（2）在.pro文件中将AdControl文件夹所在的路径添加到QML_IMPORT_PATH，以在使用AdControl模块时获得语法高亮，例如： 
```　　
    QML_IMPORT_PATH += ./  	
```
（3）在main.cpp中调用QQmlApplicationEngine::addImportPath()添加AdControl文件夹所在的qrc前缀路径，以使得系统能够加载AdControl模块的源文件，例如:  
```
    QQmlApplicationEngine engine;  
    engine.addImportPath(QStringLiteral("qrc:/"));   
```
（4）在qml文件中使用import导入AdControl模块，并声明所需的控件，例如：  
```
    import AdControl 1.0

    AdButton{
        text: "AdButton"
        height: 40
        anchors.centerIn: parent
    }
```

### 3 注意事项
初次引入模块时编译报错，可能是由于QtCreator未正确识别模块，此时可按以下步骤处理：
* 确保按照上述步骤在项目中引入了AdControl模块。
* 在QtCreator中对项目执行qmake，刷新文件依赖。
* 在QtCreator中执行 “工具——QML——重置代码模型”，重新导入QML模块。
* 在QtCreator中对项目执行重新构建，重新编译项目。

### 4 控件介绍
#### 4.1 表单控件（Forms）
 - **AdButton**  
 ![AdButton](Test/images/AdButton.gif)
 - **AdCheckButton**  
![AdCheckButton](Test/images/AdCheckButton.gif)  
 - **AdRadioButton**  
![AdRadioButton](Test/images/AdRadioButton.gif)    
 - **AdIconButton**  
![AdIconButton](Test/images/AdIconButton.gif)     
 - **AdSwitchButton**  
![AdSwitchButton](Test/images/AdSwitchButton.gif)    
 - **AdComboBox**  
![AdComboBox](Test/images/AdComboBox.gif)     
 - **AdNumberInput**  
![AdNumberInput](Test/images/AdNumberInput.gif)    
 - **AdTextInput**  
![AdTextInput](Test/images/AdTextInput.gif)    
 - **AdCalendar**  
![AdCalendar](Test/images/AdCalendar.gif)         
  
#### 4.2 弹出控件（Popups）
 - **AdDialog**  
 ![AdDialog](Test/images/AdDialog.gif)
  - **AdMenu**  
 ![AdMenu](Test/images/AdMenu.gif)
  - **AdToast**  
 ![AdToast](Test/images/AdToast.gif)
  - **AdTooltip**  
 ![AdTooltip](Test/images/AdTooltip.gif)

#### 4.3 图表控件（Charts）
  - **AdMapChart**  
 ![AdMapChart](Test/images/AdMapChart.gif)
 - **AdBarChart**  
 ![AdBarChart](Test/images/AdBarChart.gif)
  - **AdLineChart**  
 ![AdLineChart](Test/images/AdLineChart.gif)
  - **AdPieChart**  
 ![AdPieChart](Test/images/AdPieChart.gif)
  - **AdWavyProgress**  
 ![AdWavyProgress](Test/images/AdWavyProgress.gif)
  - **AdCircularProgress**  
 ![AdCircularProgress](Test/images/AdCircularProgress.gif)
  - **AdLineProgress**  
 ![AdLineProgress](Test/images/AdLineProgress.gif)

#### 4.4 工具控件（Utils）
  - **AdSideNavigationBar**  
 ![AdSideNavigationBar](Test/images/AdSideNavigationBar.gif)
 - **AdBulletinBoard**  
 ![AdBulletinBoard](Test/images/AdBulletinBoard.gif)
  - **AdScrollText**  
 ![AdScrollText](Test/images/AdScrollText.gif)
  - **AdBusyIndicator**  
 ![AdBusyIndicator](Test/images/AdBusyIndicator.gif)
  - **AdFloatingItem**  
 ![AdFloatingItem](Test/images/AdFloatingItem.gif)

#### 4.5 视图控件（Views）
  - **AdTableView**  
 ![AdTableView](Test/images/AdTableView.gif)
  - **AdTabView**  
 ![AdTabView](Test/images/AdTabView.gif)


