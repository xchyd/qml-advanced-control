﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../style.js" as Style

/*
    组合框（下拉列表）控件
*/
ComboBox{
    id:controlRoot

    //输入框样式
    property color  boxIndicatorColor:boxBorderColor
    property color  boxBorderColor:Style.color_borderGray
    property color  boxTextColor:Style.color_textBlack

    //下拉列表项目样式
    property color  itemBgNormalColor:"white"
    property color  itemBgHighlightedColor:Style.color_flatBlue
    property color  itemTextNormalColor:Style.color_textBlack
    property color  itemTextHighlightedColor:"white"
    property int    itemTextSize:boxTextSize

    //其他样式
    property bool   enableAnimation:true  //显示动画效果

    editable: false
    leftPadding: 10
    width:Style.size_bodyText*7
    height: Style.size_bodyText*2
    font.pixelSize: Style.size_bodyText
    font.family: Style.string_textFamily

    Connections{
        target: controlRoot
        onPressedChanged : {
            if(enableAnimation){
                animation.to= canvas.rotation===-180?0:-180
                animation.start()
            }
        }
    }

    PropertyAnimation {

        id:animation
        target: canvas
        property: "rotation"
        duration: 200
    }

    onActivated:{
        if(enableAnimation){
            animation.to= canvas.rotation===-180?0:-180
            animation.start()
        }
    }

    //输入框背景
    background: Rectangle {
        implicitWidth: 120
        implicitHeight: 40
        border.color: boxBorderColor
        border.width: 1
        radius: 2
    }

    //输入框文本样式
    contentItem: Text{
        width: parent.width- controlRoot.indicator.width-controlRoot.spacing
        leftPadding: controlRoot.leftPadding
        text: controlRoot.displayText
        font: controlRoot.font
        color: boxTextColor
        verticalAlignment: Text.AlignVCenter
    }

    //指示器
    indicator: Item{
        width: controlRoot.height
        height: controlRoot.height
        anchors.right: parent.right

        Canvas {
            id: canvas
            antialiasing: true
            width: parent.height*0.3
            height: parent.height*0.2
            anchors.centerIn: parent
            contextType: "2d"
            clip: true

            onPaint: {
                var context = canvas.getContext('2d');
                context.clearRect(0,0,canvas.width,canvas.height)
                context.strokeStyle = boxIndicatorColor
                context.lineWidth = 1.3;
                context.beginPath();
                context.moveTo(0, 0);
                context.lineTo(Math.floor(canvas.width/2), Math.floor(canvas.height));
                context.lineTo(Math.floor(canvas.width), 0);
                context.stroke();
            }


        }

    }

    //下拉列表项目样式(即ListView的delegate样式)
    delegate: ItemDelegate {
        width: controlRoot.width-2
        contentItem: Text {
            text: controlRoot.textRole===""?modelData:modelData[controlRoot.textRole]
            color: highlighted?itemTextHighlightedColor:itemTextNormalColor
            font: controlRoot.font
            elide: Text.ElideRight
            verticalAlignment: Text.AlignVCenter
            leftPadding:controlRoot.leftPadding
        }
        background: Rectangle{
            color:highlighted?itemBgHighlightedColor:itemBgNormalColor
        }
        highlighted: controlRoot.highlightedIndex===index
    }

    //下拉列表样式
    popup: Popup {
        id:popup
        y: controlRoot.height
        width: controlRoot.width
        implicitHeight:contentItem.implicitHeight
        padding: popupBack.border.width

        contentItem: ListView {
            id:listView
            clip: true
            implicitHeight: contentHeight+2   //略微增大高度，以避免出现滚动条
            model: controlRoot.popup.visible ? controlRoot.delegateModel : null
            currentIndex: controlRoot.highlightedIndex
            ScrollIndicator.vertical: ScrollIndicator {}
        }

        background: Rectangle {
            id:popupBack
            anchors.fill: parent
            border.color: boxBorderColor
            border.width: 1
            radius: 2
        }

        enter: Transition {
            PropertyAnimation {
                property: "y";
                from: controlRoot.height+20
                to:controlRoot.height
                duration: 250
            }
        }
    }
}
