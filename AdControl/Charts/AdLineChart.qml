﻿import QtQuick 2.14
import QtCharts 2.14
import QtQuick.Controls 2.14

/*
    折线图控件
*/
ChartView {
    id:controlRoot

    //数据模型
    //[
    //  {"color":"","label":"","value":[{x:,y:}]}   //一条数据代表一条曲线
    //  {"color":"","label":"","value":[{x:,y:}]}   //color为该线的颜色，label为该线的名称，value为该线上的每个点，x可以为Date或Number，y必须为Number
    //]
    property var    model:[]
    property string type:"spline"   //线的类型：spline(曲线)、line（折线）

    //线条样式
    property bool   pointVisible:true                //是否显示数据点
    property bool   pointLabelVisble:true            //是否显示线上的数据点
    property string pointLabelFontFamily:"微软雅黑"   //数据点标签字体族
    property int    pointLabelFontSize:12            //数据点标签字体大小
    property color  pointLabelColor:"black"          //数据点标签字体颜色
    property string pointLabelFormat:"(@xPoint, @yPoint)" //数据点标签格式（@xPoint表示对x值的引用）

    //坐标轴
    property string xAxisType:"value"       //x轴类型：value，date
    property alias  yValueAxis:yValueAxis   //y轴引用
    property alias  xValueAxis:xValueAxis   //x轴引用（type为value时有效）
    property alias  xDateAxis:xDateAxis     //x轴引用（type为data时有效）

    //信号
    //由于XYSeries的pointHoverd和pointClicked信号提供的
    //point数据不准确，因此暂未提供响应信号

    //图例样式
    legend.alignment:Qt.AlignBottom
    legend.visible: true
    legend.font.pixelSize: pointLabelFontSize
    legend.font.family: pointLabelFontFamily
    legend.markerShape:Legend.MarkerShapeRectangle

    width: 400
    height: 300
    antialiasing: true
    animationDuration: 500
    dropShadowEnabled: false
    backgroundRoundness:0
    animationOptions: ChartView.AllAnimations
    theme:ChartView.ChartThemeLight

    onModelChanged: {
        controlRoot.removeAllSeries()

        if(!Array.isArray(model)){
            return
        }

        if(type=="spline"){
            for(var i1=0;i1<model.length;i1++){
                var lineSeries1
                if(xAxisType==="date"){
                    lineSeries1 = controlRoot.createSeries(ChartView.SeriesTypeSpline,"line"+i1,xDateAxis,yValueAxis)
                }else{
                    lineSeries1 = controlRoot.createSeries(ChartView.SeriesTypeSpline,"line"+i1,xValueAxis,yValueAxis)
                }

                lineSeries1.color=model[i1].color
                lineSeries1.pointsVisible =pointVisible
                lineSeries1.pointLabelsColor=pointLabelColor
                lineSeries1.pointLabelsFont.family = pointLabelFontFamily
                lineSeries1.pointLabelsFont.pixelSize = pointLabelFontSize
                lineSeries1.pointLabelsFormat = pointLabelFormat
                lineSeries1.pointLabelsVisible = pointLabelVisble

                var modelValue1 = model[i1].value
                for(var a1 in modelValue1){
                   lineSeries1.append(modelValue1[a1].x,modelValue1[a1].y)
                }
            }
        }else{
            for(var i2=0;i2<model.length;i2++){
                var lineSeries2
                if(xAxisType==="date"){
                    lineSeries2 = controlRoot.createSeries(ChartView.SeriesTypeLine,"line"+i2,xDateAxis,yValueAxis)
                }else{
                    lineSeries2 = controlRoot.createSeries(ChartView.SeriesTypeLine,"line"+i2,xValueAxis,yValueAxis)
                }

                lineSeries2.color=model[i2].color
                lineSeries2.pointsVisible =pointVisible
                lineSeries2.pointLabelsColor=pointLabelColor
                lineSeries2.pointLabelsFont.family = pointLabelFontFamily
                lineSeries2.pointLabelsFont.pixelSize = pointLabelFontSize
                lineSeries2.pointLabelsFormat = pointLabelFormat
                lineSeries2.pointLabelsVisible = pointLabelVisble

                var modelValue2 = model[i2].value
                for(var a2 in modelValue2){
                   lineSeries2.append(new Date(modelValue2[a2].x),modelValue2[a2].y)
                }
            }
        }
    }

    ValueAxis{
        id:yValueAxis
        gridVisible:true
        min:0
        max:4
        tickCount:5
        labelFormat: "%d"
        labelsFont.pixelSize: pointLabelFontSize
        labelsFont.family: pointLabelFontFamily
    }

    ValueAxis{
        id:xValueAxis
        gridVisible:true
        min:0
        max:4
        tickCount:5
        labelFormat: "%d"
        labelsFont.pixelSize: pointLabelFontSize
        labelsFont.family: pointLabelFontFamily
    }

    DateTimeAxis {
        id:xDateAxis
        gridVisible:true
        min:new Date()
        max:new Date()
        format: "MM-dd"
        tickCount:5
        labelsFont.pixelSize: pointLabelFontSize
        labelsFont.family: pointLabelFontFamily
    }

}
