﻿import QtQuick 2.14
import QtQuick.Controls 2.14

/*
    菜单项控件
*/
MenuItem{
    id:controlRoot

    property string       type: "text"      //菜单项图标类型，支持text（仅文本）、icon（图标）、check（复选框）、radio（单选框）
    property AdMenu       parentAdMenu      //父菜单
    property ButtonGroup  buttonGroup       //属组
    property url          iconUrl:""        //图标资源

    enabled: true
    checkable: true
    width: parentAdMenu.itemWidth
    height: parentAdMenu.itemHeight
    background: Item {}
    indicator: Item{}
    contentItem:Loader{
        anchors.fill: parent
        enabled: true
        sourceComponent: controlRoot.type==="text"?textComponent:
                         controlRoot.type==="icon"?iconComponent:
                         controlRoot.type==="check"?checkComponent:
                         controlRoot.type==="radio"?raidoComponent:undefined
    }

    Component{   //文本类型
        id:textComponent
        Rectangle{
            color: controlRoot.enabled?(textMouseArea.containsMouse?parentAdMenu.itemHoveredBackColor:parentAdMenu.itemNormalBackColor):parentAdMenu.itemDisabledBackColor            
            Text {
                anchors.fill: parent
                text: controlRoot.text
                font.pixelSize: parentAdMenu.itemTextSize
                font.family: parentAdMenu.itemTextFamily
                leftPadding:controlRoot.height*0.7+15
                color:  controlRoot.enabled?(textMouseArea.containsMouse?parentAdMenu.itemHoveredTextColor:parentAdMenu.itemNormalTextColor):parentAdMenu.itemDisabledTextColor
                verticalAlignment: Qt.AlignVCenter
                textFormat: Text.RichText
            }
            MouseArea{
                id:textMouseArea
                enabled: true
                hoverEnabled: true
                anchors.fill: parent
                propagateComposedEvents :true
                onPressed: {
                    triggered()
                    mouse.accepted=false                    
                }
            }
        }
    }

    Component{   //图标类型
        id:iconComponent
        Rectangle{
            color: controlRoot.enabled?(iconMouseArea.containsMouse?parentAdMenu.itemHoveredBackColor:parentAdMenu.itemNormalBackColor):parentAdMenu.itemDisabledBackColor            
            Button{
                id:icon
                width: controlRoot.height*0.7
                spacing: 0
                enabled: false
                icon.source:controlRoot.iconUrl
                icon.color: controlRoot.enabled?(iconMouseArea.containsMouse?parentAdMenu.itemHoveredIndicatorColor:parentAdMenu.itemNormalIndicatorColor):parentAdMenu.itemDisabledIndicatorColor
                icon.height: controlRoot.height*0.4
                icon.width:controlRoot.height*0.4
                display:Button.IconOnly
                background: Item{}
                anchors.left: parent.left
                anchors.leftMargin: 5
                anchors.verticalCenter: parent.verticalCenter
            }
            Text{
                anchors.left: icon.right
                anchors.leftMargin: 10
                width: parent.width-icon.width-15
                height: parent.height
                text: controlRoot.text
                color:  controlRoot.enabled?(iconMouseArea.containsMouse?parentAdMenu.itemHoveredTextColor:parentAdMenu.itemNormalTextColor):parentAdMenu.itemDisabledTextColor
                verticalAlignment: Qt.AlignVCenter
                font.pixelSize: parentAdMenu.itemTextSize
                font.family: parentAdMenu.itemTextFamily
                textFormat: Text.RichText
            }
            MouseArea{
                id:iconMouseArea
                enabled: true
                hoverEnabled: true
                anchors.fill: parent
                propagateComposedEvents :true
                onPressed: {
                    triggered()
                    mouse.accepted=false
                }
            }
        }
    }

    Component{  //复选框类型
        id:checkComponent
        Rectangle{
            color: controlRoot.enabled?(checkMouseArea.containsMouse?parentAdMenu.itemHoveredBackColor:parentAdMenu.itemNormalBackColor):parentAdMenu.itemDisabledBackColor          
            CheckBox{
                id:checkBox
                padding: 0
                spacing: 0
                width: controlRoot.height*0.7
                indicator.width:controlRoot.height*0.4
                indicator.height:controlRoot.height*0.4
                anchors.left: parent.left
                anchors.leftMargin: 5
                anchors.verticalCenter: parent.verticalCenter
                hoverEnabled: false
                enabled: false
                checked: controlRoot.checked
                text: ""
                palette.window: controlRoot.enabled?(checkMouseArea.containsMouse?parentAdMenu.itemHoveredIndicatorColor:parentAdMenu.itemNormalIndicatorColor):parentAdMenu.itemDisabledIndicatorColor
                palette.text:  controlRoot.enabled?(checkMouseArea.containsMouse?parentAdMenu.itemHoveredIndicatorColor:parentAdMenu.itemNormalIndicatorColor):parentAdMenu.itemDisabledIndicatorColor
                palette.base: "transparent"
            }
            Text{
                anchors.left: checkBox.right
                anchors.leftMargin: 10
                width: parent.width-checkBox.width-15
                height: parent.height
                text: controlRoot.text
                color:  controlRoot.enabled?(checkMouseArea.containsMouse?parentAdMenu.itemHoveredTextColor:parentAdMenu.itemNormalTextColor):parentAdMenu.itemDisabledTextColor
                verticalAlignment: Qt.AlignVCenter
                font.pixelSize: parentAdMenu.itemTextSize
                font.family: parentAdMenu.itemTextFamily
                textFormat: Text.RichText
            }
            MouseArea{
                id:checkMouseArea
                enabled: true
                hoverEnabled: true
                anchors.fill: parent
                propagateComposedEvents :true
                onPressed: {
                    controlRoot.checked=!controlRoot.checked
                    triggered()
                    mouse.accepted=false
                }
            }
        }
    }

    Component{  //单选类型
        id:raidoComponent
        Rectangle{
            color: controlRoot.enabled?(radioMouseArea.containsMouse?parentAdMenu.itemHoveredBackColor:parentAdMenu.itemNormalBackColor):parentAdMenu.itemDisabledBackColor            
            RadioButton{
                id:radioButton
                padding: 0
                spacing: 0
                width:controlRoot.height*0.7
                height: controlRoot.height*0.7
                anchors.left: parent.left
                anchors.leftMargin: 5
                anchors.verticalCenter: parent.verticalCenter
                text: ""
                ButtonGroup.group: buttonGroup
                hoverEnabled: false
                enabled: false
                checked: controlRoot.checked
                checkable: true

                indicator: Item {
                    id:indicatorBorder
                    width: radioButton.height
                    height: width
                    y: radioButton.height / 2 - height / 2

                    Rectangle {
                        id:indicatorDot
                        width: parent.height*0.4
                        height: width
                        anchors.centerIn: parent
                        radius: height/2
                        color: controlRoot.enabled?(radioMouseArea.containsMouse?parentAdMenu.itemHoveredIndicatorColor:parentAdMenu.itemNormalIndicatorColor):parentAdMenu.itemDisabledIndicatorColor
                        visible: controlRoot.checked
                    }
                }
            }
            Text{
                anchors.left: radioButton.right
                anchors.leftMargin: 10
                width: parent.width-radioButton.width-15
                height: parent.height
                text: controlRoot.text
                color:  controlRoot.enabled?(radioMouseArea.containsMouse?parentAdMenu.itemHoveredTextColor:parentAdMenu.itemNormalTextColor):parentAdMenu.itemDisabledTextColor
                verticalAlignment: Qt.AlignVCenter
                font.pixelSize: parentAdMenu.itemTextSize
                font.family: parentAdMenu.itemTextFamily
                textFormat: Text.RichText
            }
            MouseArea{
                id:radioMouseArea
                enabled: true
                hoverEnabled: true
                anchors.fill: parent
                propagateComposedEvents :true
                onPressed: {
                    controlRoot.checked=!controlRoot.checked
                    triggered()
                    mouse.accepted=false   
                }
            }
        }
    }
}
