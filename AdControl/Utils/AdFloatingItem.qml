﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

/*
    浮动控件
*/
Item {
    id:controlRoot

    property bool dockOnLeftEnabel:true                               //是否允许左侧停靠
    property bool dockOnRightEnabel:true                              //是否允许右侧停靠
    property int  dockOnLeftCheckDistance:controlRoot.width           //右侧停靠检查距离
    property int  dockOnRightCheckDistance:controlRoot.width          //右侧停靠检查距离
    property int  dockOnLeftOffset:controlRoot.width*0.8              //停靠在左侧时偏移的距离
    property int  dockOnRightOffset:controlRoot.width*0.8             //停靠在右侧时偏移的距离

    readonly property bool isInDockLeftState:__isInDockLeftState      //指示当前是否停靠在左侧
    readonly property bool isInDockRightState:__isInDockRightState    //指示当前是否停靠在右侧

    signal  clicked(var mouse)      //单击时按下鼠标后触发
    signal  released(var mouse)     //单击时释放鼠标后触发（位于clicked之后）

    //内部数据
    property bool __isInDockLeftState:false
    property bool __isInDockRightState:false

    width: 100
    height: 100

    NumberAnimation on x{
        id:animation
        duration: 400
        easing.type: Easing.OutCubic
    }

    PropertyAnimation{
        id:hoveredAnimation
        target: controlRoot
        duration: 200
        easing.type: Easing.OutCubic
        property: "x"
    }


    Drag.active: dragArea.drag.active

    MouseArea {
        id: dragArea
        anchors.fill: parent

        drag.target: parent
        drag.maximumY: controlRoot.parent.height-controlRoot.height
        drag.minimumY: 0
        drag.maximumX: controlRoot.parent.width-controlRoot.width
        drag.minimumX: 0
        hoverEnabled: true
        z:5    //截取所有鼠标事件

        onEntered: {
            if(pressed){
                return
            }

            if(isInDockLeftState){
                if(!hoveredAnimation.running){
                    hoveredAnimation.to = 0
                    hoveredAnimation.start()
                }
            }else if(isInDockRightState){
               if(!hoveredAnimation.running){
                    hoveredAnimation.to = controlRoot.parent.width-controlRoot.width
                    hoveredAnimation.start()
                }
            }
        }

        onExited: {
            if(pressed){
                return
            }

            if(isInDockLeftState){
                if(!hoveredAnimation.running){
                    hoveredAnimation.to = -dockOnLeftOffset
                    hoveredAnimation.start()
                }
            }else if(isInDockRightState){
                 if(!hoveredAnimation.running){
                    hoveredAnimation.to = controlRoot.parent.width-controlRoot.width + dockOnRightOffset
                    hoveredAnimation.start()
                }
            }
        }

        onReleased: {

            if(!drag.active){   //区分鼠标单击释放和拖拽释放
                return
            }

            if(controlRoot.x < dockOnLeftCheckDistance){   //靠左停靠
                if(dockOnLeftEnabel){
                    animation.to = -dockOnLeftOffset
                    animation.start()
                    __isInDockLeftState = true
                    __isInDockRightState = false
                    return
                }
            }else if(controlRoot.parent.width - controlRoot.x - controlRoot.width < dockOnRightCheckDistance){  //靠右停靠
                if(dockOnRightEnabel){
                    animation.to = controlRoot.parent.width-controlRoot.width + dockOnRightOffset
                    animation.start()

                    __isInDockLeftState = false
                    __isInDockRightState = true
                    return
                }
            }

            __isInDockLeftState = false
            __isInDockRightState = false

            controlRoot.released(mouse)
        }

        onClicked: {
            controlRoot.clicked(mouse)
        }
    }
}
