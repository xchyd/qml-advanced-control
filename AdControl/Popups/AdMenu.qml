﻿import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Extras 1.4
import "../style.js" as Style

/*
    菜单控件
*/
Menu {
    id: controlRoot

    //菜单项背景颜色
    property color itemNormalBackColor:"white"
    property color itemHoveredBackColor:Style.color_flatBlue
    property color itemDisabledBackColor: itemNormalBackColor

    //菜单项文本
    property color itemNormalTextColor:Style.color_textBlack
    property color itemHoveredTextColor:"white"
    property color itemDisabledTextColor:itemNormalTextColor
    property int   itemTextSize:Style.size_bodyText
    property string itemTextFamily:Style.string_textFamily

    //菜单项指示器颜色（图标、复选框、单选框）
    property color itemNormalIndicatorColor:Style.color_borderGray
    property color itemHoveredIndicatorColor:"white"
    property color itemDisabledIndicatorColor:itemNormalIndicatorColor

    //菜单项（子菜单）箭头的颜色
    property color itemNormalArrowColor:Style.color_borderGray
    property color itemHovereArrowColor:itemNormalArrowColor
    property color itemDisabledArrowColor:itemNormalArrowColor

    //菜单项的高度和宽度，这将同时影响菜单项的字体、指示器以及箭头大小
    property int   itemWidth:itemTextSize*9
    property int   itemHeight:itemTextSize*2

    //菜单背景和边框
    property color menuBorderColor:Style.color_borderGray
    property color menuBackColor:"white"

    padding: 1

    delegate:MenuItem{  //子菜单类型
        id:menuItem
        enabled: true
        width: itemWidth
        height: itemHeight
        indicator: Item{}
        arrow:Item{}
        contentItem:Rectangle{
            anchors.fill: parent
            color: menuItem.enabled?(subMouseArea.containsMouse?itemHoveredBackColor:
                  (menuItem.subMenu.opened?itemHoveredBackColor:itemNormalBackColor)):itemDisabledBackColor          
            Text {
                width: parent.width-parent.height
                height: parent.height
                anchors.left: parent.left
                text: menuItem.text
                font.pixelSize: itemTextSize
                font.family: itemTextFamily
                leftPadding:menuItem.height*0.7+15
                color:menuItem.enabled?(subMouseArea.containsMouse?itemHoveredTextColor:
                      (menuItem.subMenu.opened?itemHoveredTextColor:itemNormalTextColor)):itemDisabledTextColor
                verticalAlignment: Qt.AlignVCenter
            }
            Canvas{
                id:subCanvas
                width: parent.height
                height: parent.height
                anchors.right: parent.right
                visible: true
                onPaint: {
                    var ctx = getContext("2d")
                    ctx.fillStyle = menuItem.enabled?(subMouseArea.containsMouse?itemHovereArrowColor:
                                     (menuItem.subMenu.opened?itemHovereArrowColor:itemNormalArrowColor)):itemDisabledArrowColor
                    ctx.moveTo(subCanvas.width*0.35, subCanvas.width*0.35)
                    ctx.lineTo(subCanvas.width-subCanvas.width*0.35, subCanvas.height/2)
                    ctx.lineTo(subCanvas.width*0.35, subCanvas.height-subCanvas.width*0.35 )
                    ctx.closePath()
                    ctx.fill()
                }
            }
            MouseArea{
                id:subMouseArea
                enabled: true
                hoverEnabled: true
                anchors.fill: parent
                propagateComposedEvents :true
            }
        }
    }

    background: Rectangle {
        id:menuBack
        implicitWidth: itemWidth
        implicitHeight: itemHeight
        color: menuBackColor
        border.color: menuBorderColor
    }
}
