﻿import QtQuick.Controls 2.12
import QtQuick 2.12
import "../style.js" as Style

/*
    复选框控件
*/
CheckBox {
    id: controlRoot

    //指示器样式
    property  color  indicatorBgCheckedColor:Style.color_flatBlue         //指示器背景三态颜色
    property  color  indicatorBgUncheckedColor:"white"
    property  color  indicatorBgDisabledColor:"white"
    property  color  indicatorBorderCheckedColor:indicatorBgCheckedColor  //指示器边框三态颜色
    property  color  indicatorBorderUncheckedColor:Style.color_borderGray
    property  color  indicatorBorderDisabledColor:Style.color_borderGray
    property  color  indicatorTickEnableColor:indicatorBgUncheckedColor    //指示器勾号颜色
    property  color  indicatorTickDisabledColor:Style.color_borderGray
    property  real   indicatorTickLineWidth:2                              //指示器勾号线宽

    //文本样式
    property  color  buttonTextCheckedColor:indicatorBgCheckedColor           //文本三态颜色
    property  color  buttonTextUnCheckedColor:Style.color_textBlack
    property  color  buttonTextDisabledColor:Style.color_borderGray

    height: Style.size_bodyText*2
    font.pixelSize: Style.size_bodyText
    font.family: Style.string_textFamily

    indicator: Rectangle {
        id:indicatorBack
        width: controlRoot.height*0.65
        height:  controlRoot.height*0.65
        x: controlRoot.leftPadding
        y: controlRoot.height / 2 - height / 2
        radius: 3
        color:controlRoot.enabled?(controlRoot.checked?indicatorBgCheckedColor:indicatorBgUncheckedColor):indicatorBgDisabledColor
        border.color: controlRoot.enabled?(controlRoot.checked?indicatorBorderCheckedColor:indicatorBorderUncheckedColor):indicatorBorderDisabledColor

        Canvas {
            id: tickCanvas
            width: indicatorBack.width*0.7
            height: indicatorBack.width*0.65
            anchors.centerIn: parent
            contextType: "2d"

            Connections {
                target: controlRoot
                onCheckedChanged: tickCanvas.requestPaint()
            }

            onPaint: {
                var context = getContext("2d");
                context.clearRect(0,0,tickCanvas.width,tickCanvas.height);

                if(controlRoot.checked)
                {
                    context.lineJoin="round"
                    context.lineCap="round"
                    context.lineWidth = indicatorTickLineWidth
                    context.strokeStyle = controlRoot.enabled?indicatorTickEnableColor:indicatorTickDisabledColor
                    context.beginPath();
                    context.moveTo(indicatorTickLineWidth, tickCanvas.height/2);
                    context.lineTo(tickCanvas.width/2-indicatorTickLineWidth/2, tickCanvas.height-indicatorTickLineWidth);
                    context.lineTo(tickCanvas.width-indicatorTickLineWidth, indicatorTickLineWidth);
                    context.stroke();
                }
            }
        }
    }

    contentItem: Text {
        text: controlRoot.text
        font: controlRoot.font
        color:controlRoot.enabled?(controlRoot.checked?buttonTextCheckedColor:buttonTextUnCheckedColor):buttonTextDisabledColor
        verticalAlignment: Text.AlignVCenter
        leftPadding: controlRoot.indicator.width + controlRoot.spacing
    }
}
