﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import "../style.js" as Style

/*
    数字输入框
*/
SpinBox{
    id:controlRoot

    //输入框数据
    property string type:"int"      //输入框类型：int(整数选择器)、double(小数选择器)
    property int    decimals: 1     //允许小数位，此属性仅在type为double时有效
    readonly property  real  realValue: controlRoot.value / (10**decimals)  //小数值需要从此属性读取，整数值直接从value读取

    //输入框样式
    property color inputTextNormalColor:Style.color_textBlack
    property color inputTextSelectedColor:"white"
    property color inputTextSelectionColor:Style.color_flatBlue
    property color inputBorderNormalColor:Style.color_borderGray
    property color inputBorderActiveColor:Style.color_borderGray
    property int   inputBorderRadius:8
    property color indicatorBgColor:"white"

    height: Style.size_bodyText*2
    width: Style.size_bodyText*7
    from: 0
    to: 9999
    stepSize: 1
    validator:controlRoot.type==="double"?doubleValidator:intValidator
    textFromValue: controlRoot.type==="double"?doubleValidator.textFromValue:intValidator.textFromValue
    valueFromText: controlRoot.type==="double"?doubleValidator.valueFromText:intValidator.valueFromText
    editable: true
    opacity: enabled?1:0.5
    font.pixelSize:Style.size_bodyText
    font.family: Style.string_textFamily
    clip: true

    contentItem: TextInput {
        z: 2
        text: controlRoot.displayText
        font: controlRoot.font
        color: inputTextNormalColor
        selectionColor: inputTextSelectionColor
        selectedTextColor:inputTextSelectedColor
        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter
        selectByMouse: true
        readOnly: !controlRoot.editable
        validator: controlRoot.validator
        inputMethodHints: Qt.ImhFormattedNumbersOnly
    }

    up.indicator: Item {
        id:upItem
        x: controlRoot.mirrored ? 0 : parent.width - width
        height: parent.height
        width: parent.height
        clip: true

        Rectangle {
            id:upBg
            height: parent.height-2
            width: parent.width-2
            anchors.centerIn: parent
            color:upItem.enabled?(controlRoot.up.pressed ? Qt.darker(indicatorBgColor,1.2 ): indicatorBgColor):Qt.lighter(indicatorBgColor,1.5 )
            radius: inputBorderRadius

            Text {
                text: "+"
                font.pixelSize: controlRoot.font.pixelSize+6
                color: upItem.enabled?Qt.darker(inputBorderNormalColor,2):inputBorderNormalColor
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                renderType: Text.NativeRendering
            }
        }

        Rectangle{
            width: inputBorderRadius
            height: parent.height-2
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            color: upBg.color
            Rectangle{
                width: 1
                height: parent.height
                color:controlRoot.activeFocus?inputBorderActiveColor:inputBorderNormalColor
                anchors.left: parent.left
            }
        }
    }

    down.indicator:  Item {
        id:downItem
        height: parent.height
        width: parent.height
        clip: true
        Rectangle {
            id:downBg
            height: parent.height-2
            width: parent.width-2
            anchors.centerIn: parent
            color:downItem.enabled?(controlRoot.down.pressed ? Qt.darker(indicatorBgColor,1.2 ): indicatorBgColor):Qt.lighter(indicatorBgColor,1.5 )
            radius: inputBorderRadius
            clip: false

            Text {
                text: "-"
                font.pixelSize: controlRoot.font.pixelSize+5
                color:downItem.enabled?Qt.darker(inputBorderNormalColor,2):inputBorderNormalColor
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                renderType: Text.NativeRendering
            }
        }

        Rectangle{
            width: inputBorderRadius
            height: parent.height-2
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            color: downBg.color
            Rectangle{
                width: 1
                height: parent.height
                color:controlRoot.activeFocus?inputBorderActiveColor:inputBorderNormalColor
                anchors.right: parent.right
            }
        }
    }

    background: Rectangle {
        z:6
        border.color: controlRoot.activeFocus?inputBorderActiveColor:inputBorderNormalColor
        radius: inputBorderRadius
        clip: true
        color:"transparent"
    }

    IntValidator{
        id:intValidator

        bottom: Math.min(controlRoot.from, controlRoot.to)
        top:  Math.max(controlRoot.from, controlRoot.to)

        //数值变化，在需要更新输入框的文本时调用
        function textFromValue(value, locale) {
            return value
        }

        //输入框文本变化，在需要更新数值时调用
        function valueFromText(text, locale) {
            var lastValue = controlRoot.value

            if(text===""){    //输入不合法时保持原来的值不变
                controlRoot.displayTextChanged()
                return lastValue
            }else{
                var value = Number.fromLocaleString(locale, text)
                if(value<controlRoot.from || value>controlRoot.to){ //输入越界时保持原来的值不变
                    controlRoot.displayTextChanged()
                    return lastValue
                }else{
                    return value
                }
            }
        }

    }

    DoubleValidator {
        id:doubleValidator

        bottom: Math.min(controlRoot.from, controlRoot.to)
        top:  Math.max(controlRoot.from, controlRoot.to)

        //数值变化，在需要更新输入框的文本时调用
        function textFromValue(value, locale) {
            var text = Number(value / Math.pow(10,controlRoot.decimals)).toLocaleString(locale, 'f', controlRoot.decimals)
            return text
        }

        //输入框文本变化，在需要更新数值时调用
        function valueFromText(text, locale) {
            var lastValue = controlRoot.value

            if(text===""){    //输入不合法时保持原来的值不变
                controlRoot.displayTextChanged()
                return lastValue
            }else{
                var strNumber = Number.fromLocaleString(locale, text).toFixed(controlRoot.decimals) + ""  //将输入文本转换为固定小数位数的字符串
                var value  = strNumber.replace(".","")   //去掉小数的字符串

                if(value<controlRoot.from || value>controlRoot.to){ //输入越界时保持原来的值不变
                    controlRoot.displayTextChanged()
                    return lastValue
                }else{
                    return value
                }
            }
        }
    }
}
