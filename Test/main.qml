﻿import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls 1.4 as Control1
import QtQuick.Layouts 1.12
import QtCharts 2.14

import AdControl 1.0

Window {
    id:root
    width: 1200
    height: width/9*6
    visible: true
    title: qsTr("AdControl Demo     作者:西渣渣     git:https://gitee.com/chen-jianli")

/*
    取消下列注释，查看相应控件的演示效果
*/

/************************  Forms  *****************************
    Column{
        anchors.centerIn: parent
        spacing: 100


//        Row{
//            spacing: 20
//            AdButton{
//                text: "简单按钮"
//                borderColor: Qt.darker(bgColor,1.25)
//                type: "plain"
//            }
//            AdButton{
//                text: "扁平化按钮"
//                type:"flat"
//            }
//            AdButton{
//                text: "轮廓按钮"
//                type: "outline"
//            }
//        }


//        Row{
//            spacing: 20
//            AdIconButton{
//                id:iconButton

//                checked: false
//                iconCheckedSource: "/icon/total-face.svg"
//                iconUncheckedSource: "/icon/total-line.svg"
//            }
//            Text{
//                text: iconButton.checked?"图标按钮（选中态）":"图标按钮（未选中态）";
//                font.pixelSize: 18;font.family: "微软雅黑";height: 36;verticalAlignment: Text.AlignVCenter
//            }
//        }

//        AdCheckButton{
//            checked: false
//            text: checked?"复选框（选中态）":"复选框（未选中态）"
//        }

//        AdRadioButton{
//            checked: false
//            text:  checked?"单选按钮（选中态）":"单选按钮（未选中态）"
//        }



//        AdSwitchButton{
//            checked: false
//            text:checked?"开关按钮（选中态）":"开关按钮（未选中态）"
//        }

//        AdComboBox{
//            model: ["项目 1","项目 2","项目 3"]
//        }

//        AdNumberInput{
//            type:"double"
//        }

//        AdTextInput{
//            iconVisible: true
//            iconUrl: "/icon/total-face.svg"
//            text:""
//        }

//        AdCalendar{
//            visible: true
//        }
    }
**************************************************************/


/************************  Popups ****************************
        AdButton{
            id:dialogTestButton
            anchors.centerIn: parent
            text: ""
            onClicked: {
                //adDialog.open()
                //adMenu.open()
                //toast.show("AdToast!")
            }

            AdToolTip{
                text: "工具提示，悬浮时自动显示"
            }
        }

        AdDialog{
            id:adDialog
            title: "对话框"
            Rectangle{
                implicitWidth: 400
                implicitHeight: 300
            }
        }

        AdMenu{
            id:adMenu
            x:dialogTestButton.x
            y:dialogTestButton.y+dialogTestButton.height

            AdMenuItem{
                type:"text"
                parentAdMenu: adMenu
                text: "Hello Qt"
            }

            AdMenuItem{
                type:"check"
                parentAdMenu: adMenu
                text: "Hello Qt"
            }

            AdMenuItem{
                type:"radio"
                parentAdMenu: adMenu
                text: "Hello Qt"
            }

            AdMenuItem{
                type:"icon"
                parentAdMenu: adMenu
                text: "Hello Qt"
                iconUrl: "/icon/home.svg"
            }

            AdMenu{
                id:adMenu2
                title: "SubMenu"

                AdMenuItem{
                    type:"text"
                    parentAdMenu: adMenu2
                    text: "Hello Qt"
                }

            }
        }

        AdToast{
            id:toast
        }

**************************************************************/


/************************  Utils  *****************************

//    AdBulletinBoard{
//        id:board
//        title: "公告板"
//        anchors.centerIn: parent

//        Text {
//            text: "内容将自动放置到这里"
//            font.pixelSize: 18
//            anchors.centerIn: parent
//        }
//    }

//        AdBusyIndicator{
//            anchors.centerIn: parent
//        }

//        AdFloatingItem{
//            width: 50
//            height: 50

//            Rectangle{
//                color: "#20A0FF"
//                width: 50
//                height: 50
//                radius: width/2
//            }
//        }

//        AdScrollText{
//            anchors.centerIn: parent
//            text:"Hello Qt!  I'm XiZhaZha!"
//        }

//        AdSideNavigationBar{
//            width: 200
//            mainLabelNormalTextColor:"white"
//            mainLabelNormalBgColor:"#20A0FF"
//            mainLabelHoverdBgColor:Qt.darker("#20A0FF",1.1)
//            subLabelNormalBgColor:Qt.darker("#20A0FF",1.1)
//            subLabelHoverdBgColor:Qt.darker("#20A0FF",1.1)

//            height: parent.height
//            mainLabelCollapseIcon: "/icon/left.svg"
//            mainLabelExpandIcon: "/icon/down.svg"
//            model:[
//                {"main":"主标题1","subList":["子标题1","子标题2"],"icon":"/icon/home.svg"},
//                {"main":"主标题2","subList":["子标题1","子标题2"],"icon":"/icon/home.svg"}
//            ]
//            onItemClicked: {
//                console.log(label,currentSelectedLabel)
//            }
//        }

**************************************************************/


/************************  Charts  ****************************/

//    AdMapChart{
//        anchors.centerIn: parent
//        model:[
//            {"name":"广东","value":50},
//            {"name":"四川","value":40},
//            {"name":"河北","value":30},
//            {"name":"内蒙","value":40},
//            {"name":"甘肃","value":30},
//            {"name":"青海","value":30},
//            {"name":"陕西","value":30},
//            {"name":"河南","value":40},
//            {"name":"湖南","value":30},
//            {"name":"湖北","value":40},
//            {"name":"安徽","value":30},
//            {"name":"浙江","value":40},
//            {"name":"江苏","value":30},
//        ]
//    }

//    AdPieChart{
//        id:adPieChart
//        anchors.centerIn: parent
//        legend.visible: true
//        model: [
//             {"label":"pie1","value":10},
//             {"label":"pie1","value":10},
//             {"label":"pie1","value":10},
//             {"label":"pie1","value":10},
//             {"label":"pie1","value":10}
//        ]
//    }


//    AdButton{
//        onClicked: {
//           barChart.categoryAxis.categories= ["A","B","C","D","E"]
//           barChart.model= [
//               {
//                   "color":"#20A0FF",
//                   "label":"2021",
//                   "value":[2,1,3,5,4]
//               }
//           ]
//        }
//    }
//    AdBarChart{
//        id:barChart
//        anchors.centerIn: parent
//        categoryAxis.categories: ["A","B","C","D","E"]
//        model:[
//            {
//                "color":"#20A0FF",
//                "label":"2021",
//                "value":[1,2,3,4,5]
//            }
//        ]

//        onBarSetClicked: {
//            console.log(index,barset)
//        }
//    }


//    AdButton{
//        onClicked: {
//           lineChart.model=[
//               {
//                   "color":"#EC4758",
//                   "label":"2020",
//                   "value":[{"x":0,"y":4},{"x":1,"y":2},{"x":2,"y":1},{"x":3,"y":3},{"x":4,"y":1}]
//               },
//               {
//                   "color":"#F8AC59",
//                   "label":"2021",
//                   "value":[{"x":0,"y":3},{"x":1,"y":4},{"x":2,"y":3},{"x":3,"y":1},{"x":4,"y":2}]
//               }
//           ]
//        }
//    }
//        AdLineChart{
//            id:lineChart
//            anchors.centerIn: parent
//            yValueAxis.max: 5

//            model:[
//                {
//                    "color":"#20A0FF",
//                    "label":"2020",
//                    "value":[{"x":0,"y":1},{"x":1,"y":2},{"x":2,"y":3},{"x":3,"y":2},{"x":4,"y":1}]
//                },
//                {
//                    "color":"#18A689",
//                    "label":"2021",
//                    "value":[{"x":0,"y":2},{"x":1,"y":3},{"x":2,"y":4},{"x":3,"y":2},{"x":4,"y":5}]
//                }
//            ]
//    }

//    AdButton{
//        onClicked: {
//            //sphereProgress.value += 0.1
//            //circularProgress.value += 0.1
//            //adLineProgress.value +=0.1
//        }
//    }
//    AdWavyProgress{
//        id:sphereProgress
//        value:0.4
//        anchors.centerIn: parent
//        enableAnimation: true
//    }


//        AdCircularProgress{
//            id:circularProgress
//            value:0.4
//            bgRingColor:"#c0c0c0"
//            anchors.centerIn: parent
//        }

//        AdLineProgress{
//            id:adLineProgress
//            value: 0.4
//            anchors.centerIn: parent
//            //indeterminate: true
//            bgLineColor:"#c0c0c0"

//        }

/************************  Charts  ****************************/


/************************  Views  ****************************

//    AdTabView{
//        id:tabView
//        anchors.centerIn: parent
//        type:"card"

//        Control1.Tab{
//            title: "TAB 1"
//            Item{
//                anchors.fill: parent
//                Text{
//                    anchors.centerIn: parent
//                    text: title
//                    font.pixelSize: tabView.tabTextSize
//                    font.family: tabView.tabTextFamily
//                }
//            }
//        }
//        Control1.Tab{
//            title: "TAB 2"
//            Item{
//                anchors.fill: parent
//                Text{
//                    anchors.centerIn: parent
//                    text: title
//                    font.pixelSize: tabView.tabTextSize
//                    font.family: tabView.tabTextFamily
//                }
//            }
//        }
//        Control1.Tab{
//            title: "TAB 3"
//            Item{
//                anchors.fill: parent
//                Text{
//                    anchors.centerIn: parent
//                    text: title
//                    font.pixelSize: tabView.tabTextSize
//                    font.family: tabView.tabTextFamily
//                }
//            }
//        }

//    }

//    AdTableView{
//        id:adTableView
//        anchors.centerIn: parent
//        width: 100*5
//        height: 500
//        Control1.TableViewColumn{
//            title: "column1"
//            role:"column1"
//            width: 100
//            horizontalAlignment: Text.AlignHCenter
//        }
//        Control1.TableViewColumn{
//            title: "column2"
//            role:"column2"
//            width: 100
//            horizontalAlignment: Text.AlignHCenter
//        }
//        Control1.TableViewColumn{
//            title: "column3"
//            role:"column3"
//            width: 100
//            horizontalAlignment: Text.AlignHCenter
//        }
//        Control1.TableViewColumn{
//            title: "column4"
//            role:"column4"
//            width: 100
//            horizontalAlignment: Text.AlignHCenter
//        }

//        Component.onCompleted: {
//            for(var i=0;i<50;i++){
//                var data = {"column1":i,"column2":i,"column3":i,"column4":i,"checked":false}
//                adTableView.listModel.append(data)
//            }
//        }

//        onItemClick: {
//            console.log(row,column)
//        }

//        onRightButtonClicked: {
//            console.log(row,column)
//        }
//    }
************************  Charts  ****************************/

}
