﻿import QtQuick 2.14
import QtQuick.Controls 2.14
import "../style.js" as Style

/*
    文本输入框控件
*/
TextField{
    id:controlRoot

    //输入框边框
    property color  inputBorderNormalColor:Style.color_borderGray
    property color  inputBorderActiveColor:Style.color_flatBlue
    property color  inputBorderDisabledColor:inputBorderNormalColor
    property int    inputBorderActiveWidth: 1
    property int    inputBorderNormalWidth: 1
    property int    inputBorderRadius:3

    //输入框背景
    property color  inputBgNormalColor:"white"
    property color  inputBgActiveColor:inputBgNormalColor
    property color  inputBgDisabledColor:Qt.lighter(inputBgNormalColor,1.5)

    //输入框文本颜色
    property color  inputTextNormalColor:Style.color_textBlack
    property color  inputTextActiveColor:inputTextNormalColor
    property color  inputTextDisabledColor:inputBorderNormalColor

    //输入框图标
    property bool   iconVisible:false                         //是否显示图标
    property url    iconUrl:""                             //图标的资源路径
    property string iconPostion: "left"                    //图标的显示位置：left在左侧显示，其他情况均在右侧显示
    property bool   iconClickAnimation:true               //点击图标时是否显示交互动画
    property color  iconNormalColor: inputBorderNormalColor
    property color  iconHoverdColor: inputBorderNormalColor
    property color  iconDisabledColor: inputTextDisabledColor

    //信号
    signal iconClicked()     //点击图标时触发

    selectedTextColor: "white"
    selectionColor: inputBorderActiveColor

    font.pixelSize: Style.size_bodyText
    font.family: Style.string_textFamily
    width: Style.size_bodyText*8
    height:  Style.size_bodyText*2
    verticalAlignment: Qt.AlignVCenter
    selectByMouse: true
    clip:true
    color: controlRoot.enabled?(controlRoot.activeFocus?inputTextActiveColor:inputTextNormalColor):inputTextDisabledColor
    opacity:controlRoot.enabled?1:0.5
    leftPadding:iconVisible?(iconPostion==="left"?iconButton.width:10):10
    rightPadding:iconVisible?(iconPostion==="left"?10:iconButton.width):10

    background: Rectangle{
        color: controlRoot.enabled?(controlRoot.activeFocus?inputBgActiveColor:inputBgNormalColor):inputBgDisabledColor
        border.color: controlRoot.enabled?(controlRoot.activeFocus?inputBorderActiveColor:inputBorderNormalColor):inputBorderDisabledColor
        border.width: controlRoot.activeFocus?inputBorderActiveWidth:inputBorderNormalWidth
        radius: controlRoot.inputBorderRadius
    }

    Button{
        id:iconButton
        enabled: visible
        visible: iconVisible
        x:iconPostion==="left"?0:(controlRoot.width-iconButton.width)
        anchors.verticalCenter: parent.verticalCenter
        display: Button.IconOnly
        height: parent.height-4
        width:height
        icon.source:iconUrl
        icon.color: controlRoot.enabled?(iconButton.hovered?iconHoverdColor:iconNormalColor):iconDisabledColor
        background: Rectangle{
            color: "transparent"
        }
        onClicked: {
            controlRoot.iconClicked()
        }
        onPressed: {
            if(iconClickAnimation){
                iconButton.opacity=0.5
            }
        }

        onReleased: {
            if(iconClickAnimation){
                iconButton.opacity=1
            }
        }
    }

}
