﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import "../style.js"  as Style

/*
    单选按钮控件
*/
RadioButton {
    id: controlRoot

    //指示器样式
    property  color  indicatorBgCheckedColor:Style.color_flatBlue           //指示器背景三态颜色
    property  color  indicatorBgUncheckedColor:"white"
    property  color  indicatorBgDisabledColor:"white"
    property  color  indicatorBorderCheckedColor:indicatorBgCheckedColor   //指示器边框三态颜色
    property  color  indicatorBorderUncheckedColor:Style.color_borderGray
    property  color  indicatorBorderDisabledColor:Style.color_borderGray
    property  color  indicatorDotEnableColor:"white"                             //指示器圆点颜色
    property  color  indicatorDotDisabledColor:indicatorBorderDisabledColor

    //文本样式
    property  color  buttonTextCheckedColor:indicatorBgCheckedColor           //文本三态颜色
    property  color  buttonTextUnCheckedColor:Style.color_textBlack
    property  color  buttonTextDisabledColor:Style.color_borderGray

    height: Style.size_bodyText*1.5
    font.pixelSize: Style.size_bodyText
    font.family: Style.string_textFamily

    indicator: Rectangle {
        id:indicatorBorder
        width: controlRoot.height
        height: width
        x: controlRoot.leftPadding
        y: controlRoot.height / 2 - height / 2
        radius: height/2
        color:controlRoot.enabled?(controlRoot.checked?indicatorBgCheckedColor:indicatorBgUncheckedColor):indicatorBgDisabledColor
        border.color: controlRoot.enabled?(controlRoot.checked?indicatorBorderCheckedColor:indicatorBorderUncheckedColor):indicatorBorderDisabledColor

        Rectangle {
            id:indicatorDot
            width: parent.height*0.4
            height: width
            anchors.centerIn: parent
            radius: height/2
            color: controlRoot.enabled?indicatorDotEnableColor:indicatorDotDisabledColor
            visible: controlRoot.checked
        }
    }

    contentItem: Text {
        text: controlRoot.text
        font: controlRoot.font
        color:controlRoot.enabled?(controlRoot.checked?buttonTextCheckedColor:buttonTextUnCheckedColor):buttonTextDisabledColor
        verticalAlignment: Text.AlignVCenter
        leftPadding: controlRoot.indicator.width + controlRoot.spacing
    }
}
