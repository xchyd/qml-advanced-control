﻿import QtQuick 2.12
import QtGraphicalEffects 1.14
import QtQuick.Controls 1.4 as Control1
import QtQuick.Controls.Styles 1.4
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../style.js" as Style

/*
    气泡控件
*/
Popup {
    id:controlRoot

    //控件样式
    property color  bgColor:"black"                     //背景颜色
    property real   bgOpacity:0.7                       //背景透明度
    property color  textColor:"white"                   //字体颜色
    property int    textSize:Style.size_bodyText        //字体大小
    property string textFamily:Style.string_textFamily  //字体族

    //显示时间
    property int    fadeInTime:500                      //淡入时间
    property int    fadeOutTime:1000                    //淡出时间
    property int    durationTime:1000                   //持续显示时间

    x:parent.width/2-controlRoot.width/2          //相对父控件居中显示
    y:parent.height/2-controlRoot.height/2
    implicitWidth:contentText.contentWidth+50     //尺寸随文本自动变化
    implicitHeight: contentText.contentHeight+20
    closePolicy:Popup.NoAutoClose

    background: Rectangle{
        id:back
        color:bgColor
        radius: Math.ceil(controlRoot.height*0.1)
        opacity: bgOpacity
    }

    contentItem: Text {
        id:contentText
        height: parent.height
        font.pixelSize: textSize
        font.family: textFamily
        color: textColor
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    Timer{
        id:durationTimer
        interval: durationTime
        repeat: false
        onTriggered: {
            closeAnimation.start()
        }
    }

    PropertyAnimation{
        id:showAnimation
        target: controlRoot
        property: "opacity"
        to:1
        duration: fadeInTime
        onFinished: durationTimer.restart()
    }

    PropertyAnimation{
        id:closeAnimation
        target: controlRoot
        property: "opacity"
        to:0
        duration: fadeOutTime
        onFinished: controlRoot.close()
    }

    function show(message, duration){

        if(typeof(duration)===typeof(1)){
            durationTime = duration
        }

        durationTimer.stop();
        showAnimation.stop()
        closeAnimation.stop();

        contentText.text=message
        controlRoot.opacity=0
        controlRoot.open()
        showAnimation.restart()
    }
}
