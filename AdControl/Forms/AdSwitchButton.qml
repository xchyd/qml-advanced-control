﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import "../style.js" as Style

/*
    开关按钮控件
*/
Switch {
    id: controlRoot

    //按钮样式
    property color  buttonBgOnColor:Style.color_flatBlue
    property color  buttonBgOffColor:"white"
    property color  buttonBorderOnColor:buttonBgOnColor
    property color  buttonBorderOffColor:Style.color_borderGray

    //滑块样式
    property color  sliderBgOnColor:"white"
    property color  sliderBgOffColor:"white"
    property color  sliderBgDownColor:sliderBorderOffColor
    property color  sliderBorderOnColor:buttonBgOnColor
    property color  sliderBorderOffColor:Style.color_borderGray

    //文本样式
    property  color  buttonTextCheckedColor:buttonBgOnColor           //文本三态颜色
    property  color  buttonTextUnCheckedColor:Style.color_textBlack
    property  color  buttonTextDisabledColor:Style.color_borderGray

    height:Style.size_bodyText*1.5
    font.pixelSize: Style.size_bodyText
    font.family: Style.string_textFamily

    indicator: Rectangle {
        id:indicatorBack
        implicitWidth: Style.size_bodyText*3
        implicitHeight: controlRoot.height
        x: controlRoot.leftPadding
        y: parent.height / 2 - height / 2
        radius:Math.floor(indicatorBack.height/2)
        color: controlRoot.checked ? buttonBgOnColor : buttonBgOffColor
        border.color: controlRoot.checked ? buttonBorderOnColor: buttonBorderOffColor

        Rectangle {
            x: controlRoot.checked ? parent.width - width : 0
            width: indicatorBack.height
            height: indicatorBack.height
            radius: Math.floor(indicatorBack.height/2)
            color: controlRoot.checked ?(controlRoot.down ? sliderBgDownColor :sliderBgOnColor):sliderBgOffColor
            border.color: controlRoot.checked ?sliderBorderOnColor: sliderBorderOffColor

            Behavior on x {
                PropertyAnimation{
                    property: "x"
                    duration: 200
                }
            }
        }
    }

    contentItem: Text {
        text: controlRoot.text
        font: controlRoot.font
        color:controlRoot.enabled?(controlRoot.checked?buttonTextCheckedColor:buttonTextUnCheckedColor):buttonTextDisabledColor
        verticalAlignment: Text.AlignVCenter
        leftPadding: controlRoot.indicator.width + controlRoot.spacing
    }
}
