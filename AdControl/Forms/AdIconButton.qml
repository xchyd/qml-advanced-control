﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import "../style.js" as Style

/*
    图标按钮
*/
Button {
    id:controlRoot

    property color  iconCheckedColor:Style.color_flatBlue       //选中时图标颜色
    property color  iconUncheckedColor:Style.color_borderGray   //未选中时图标颜色
    property url    iconCheckedSource:""                        //选中时的图标资源
    property url    iconUncheckedSource:""                      //未选中时的图标资源
    property color  bgNormalColor:"transparent"                 //正常状态下背景颜色
    property color  bgHoverdColor:"transparent"                 //鼠标悬浮时背景颜色

    property real   __opacity:1

    display: Button.IconOnly
    width: Style.size_bodyText*2
    height: Style.size_bodyText*2
    checkable: true
    hoverEnabled: true
    icon.source:controlRoot.checked?iconCheckedSource:iconUncheckedSource
    icon.width: Math.min(controlRoot.width,controlRoot.height)
    icon.height: Math.min(controlRoot.width,controlRoot.height)
    icon.color:controlRoot.checked?iconCheckedColor:iconUncheckedColor
    opacity: controlRoot.enabled?__opacity:0.6
    onPressed: __opacity = 0.5
    onReleased: __opacity = 1

    background: Rectangle{
        color: controlRoot.hovered?bgHoverdColor:bgNormalColor
    }
}
