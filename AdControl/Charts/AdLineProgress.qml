﻿import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.12
import "../style.js" as Style

/*
    进度条控件
*/
ProgressBar {
    id: controlRoot

    //进度样式
    property int    radius:2
    property color  bgLineColor:Qt.lighter(Style.color_borderGray,1.25)
    property color  progressLineColor:Style.color_flatBlue
    property int    indeterminateAnimationDuration:1000

    //文本样式
    property bool   textVisible:true
    property int    textSize:controlRoot.height*0.5
    property color  textColor:Style.color_textBlack
    property string textFamily:Style.string_textFamily
    property string textPosition:"innerRight"    //进度值文本位置：innerCenter(内部居中)、innerRight(内部居右)


    width: 200
    height: 30
    indeterminate: false
    padding: 0

    background: Rectangle {  //背景条
        width: parent.width
        height: parent.height
        color: bgLineColor
        radius: controlRoot.radius
    }

    contentItem: Item {    //进度条
        id:progressItem
        width: parent.width
        height: parent.height*0.8
        clip:true

        Rectangle {  //进度块
            id:progressRect
            width: {
                if(controlRoot.indeterminate){
                    return parent.width*0.3
                }else{
                    return controlRoot.visualPosition * parent.width
                }
            }
            height: parent.height
            radius: controlRoot.radius
            color: progressLineColor
        }

        Text{   //进度值文本
            x:{
                if(textPosition==="innerRight"){
                    return progressItem.width - width-5
                }else{
                    return progressItem.width/2 - width/2
                }
            }
            visible: textVisible
            text: value*100+"%"
            color: textColor
            font.pixelSize: textSize
            font.family: textFamily
            anchors.verticalCenter: parent.verticalCenter
        }
    }

    PropertyAnimation{  //滚动动画
        id:animation
        target:progressRect
        property: "x"
        from:-progressRect.width
        to:controlRoot.width
        loops:Animation.Infinite
        duration: indeterminateAnimationDuration
    }

    onIndeterminateChanged: {
        if(controlRoot.indeterminate){
            animation.restart()
        }else{
            animation.stop()
            progressRect.x = 0
        }
    }

    Component.onCompleted: {
        if(controlRoot.indeterminate){
            animation.start()
        }
    }

    //开启滚动动画（仅在indeterminate为true时有效）
    function startIndeterminateAnimation(){
        if(controlRoot.indeterminate){
            animation.start()
        }
    }

    //停止滚动动画
    function stopIndeterminateAnimation(){
        animation.stop()
    }
}
