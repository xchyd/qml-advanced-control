﻿import QtQuick 2.14
import QtCharts 2.14
import QtQuick.Controls 2.14
import "../style.js" as Style

/*
    饼图控件
*/
ChartView {
    id:controlRoot

    //数据模型
    //[
    // {"color":"","label":"label1",value:20},    //一条数据对应一个饼图块
    // {"color:"",""label":"label2",value:80}     //label为饼图块标签名，value为该饼图块占比,color为该饼图块的颜色（可以不指定，此时将使用pieSliceBaseColor的渐变色填充）
    //]
    property var    model:[]

    //饼图块样式
    property color  pieSliceBaseColor:Qt.darker(Style.color_flatBlue,2)  //饼图块基础渐变颜色
    property real   pieSliceScaleSize:0.85              //饼图块缩放比例（0~1）
    property real   pieSliceholeSize:0                  //饼图块中洞大小（0~1）
    property real   pieSliceExplodeSize:0.15            //饼图块弹出距离（0~1）
    property int    pieSliceBorderWidth:0               //饼图块边框宽度
    property color  pieSliceBorderColor:"white"         //饼图块边框颜色
    property bool   pieSliceLabelVisble:true            //是否显示饼图块标签
    property color  pieSliceLabelColor:"white"          //饼图块标签字体颜色
    property string pieSliceLabelFontFamily:Style.string_textFamily  //饼图块标签的字体族
    property int    pieSliceLabelFontSize:12            //饼图块标签的字体大小
    property int    pieSliceLabelDecimals:2             //饼图块标签显示的小数精度
    property bool   pieSliceToolTipVisible:true         //是否显示饼图块工具提示

    //信号
    signal  sliceClicked(var slice)      //饼图块被点击时触发，slice保存了被点击的饼图块的对象PieSlice

    //图例样式
    legend.alignment:Qt.AlignLeft
    legend.visible: false
    legend.font.pixelSize: pieSliceLabelFontSize
    legend.font.family: pieSliceLabelFontFamily
    legend.markerShape:Legend.MarkerShapeCircle

    width: 400
    height:300
    backgroundColor: "white"
    antialiasing: true
    animationDuration: 500
    dropShadowEnabled: false
    backgroundRoundness:0
    animationOptions: ChartView.AllAnimations
    theme:ChartView.ChartThemeLight

    PieSeries {
        id: pieSeries
        size: pieSliceScaleSize
        holeSize:pieSliceholeSize
        useOpenGL: true
        onClicked: sliceClicked(slice)
        onHovered: {
            if(pieSliceToolTipVisible){
                if(state){
                    toolTip.text=slice.label
                    toolTip.popup()
                    slice.exploded =true
                }else{
                    toolTip.close()
                    slice.exploded =false
                }
            }
        }
    }

    Menu{
        id:toolTip
        property string text:""
        width: toolTipText.contentWidth+20
        height: toolTipText.contentHeight
        background: Rectangle{
            color: "black"
            radius: 5
            opacity: 0.5
        }
        Text {
            id:toolTipText
            text: toolTip.text
            font.pixelSize: pieSliceLabelFontSize
            font.family: pieSliceLabelFontFamily
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            color: "white"
        }
    }

    onModelChanged: {
        pieSeries.clear()

        if(typeof(model)==="undefined"){
            return
        }

        var colorArray = getColorArray(pieSliceBaseColor,true,model.length)
        for(var i=0;i<model.length;i++){
            var pieSlice = pieSeries.append(model[i].label,model[i].value)
            if(pieSlice){
                pieSlice.color = Qt.binding(function(){
                    if(model[i].color==="" || model[i].color===undefined){
                        return colorArray[i]
                    }else{
                        return model[i].color
                    }
                })
                pieSlice.borderWidth = pieSliceBorderWidth
                pieSlice.borderColor = Qt.binding(function(){
                    if(pieSliceBorderWidth==0){
                        return pieSlice.color
                    }else{
                        return pieSliceBorderColor
                    }
                })
                pieSlice.explodeDistanceFactor = pieSliceExplodeSize
                pieSlice.labelColor = pieSliceLabelColor
                pieSlice.labelVisible = pieSliceLabelVisble
                pieSlice.labelPosition = PieSlice.LabelInsideNormal
                pieSlice.labelFont.family=pieSliceLabelFontFamily
                pieSlice.labelFont.pixelSize=pieSliceLabelFontSize
                pieSlice.label = model[i].label+"\n"+ model[i].value.toFixed(pieSliceLabelDecimals)+"%"
            }
        }

        //console.log("\nPieChart Model:\n",JSON.stringify(model),"\n")
    }

    //返回一组与hexColor相近的颜色值。起始颜色hexColor为数组的第一个元素
    //hexColor：color,起始颜色（HTML颜色值，如"#000000")
    //lighter: bool,指定颜色变淡(true)还是变深（false）
    //count: int，指定需要返回的颜色数量
    function getColorArray(hexColor,lighter,count){
        var colorObjArray = []
        var hexColorArray = []

        var lighterFactor =  0.5//1/count
        var darkerFactor = 0.8/count
        colorObjArray.push(hexColor)
        for(var i = 1;i<=count-1;i++){
            var color ;
            if(lighter){
                color = Qt.lighter(hexColor,i*lighterFactor+1)
            }else{
                color = Qt.darker(hexColor,1/(1-darkerFactor*i))
            }
            colorObjArray.push(color+"")
        }

        return colorObjArray
    }
}

