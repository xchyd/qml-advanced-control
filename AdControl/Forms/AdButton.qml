﻿import QtQuick.Controls 2.14
import QtQuick 2.14
import "../style.js" as Style

/*
    按钮控件
*/
Button {
    id:controlRoot

    property string type:"flat"                         //按钮风格：flat(扁平化按钮，默认)、plain(普通按钮)、outline(线性按钮)
    property color  bgColor: Style.color_flatBlue       //按钮背景
    property color  textColor:"white"                   //字体颜色
    property color  borderColor:bgColor                 //边框颜色
    property int    borderRadius:controlRoot.height/8   //边框圆角弧度

    property real   __opacity:1

    icon.height: controlRoot.height*0.5
    icon.width: controlRoot.height*0.5
    icon.color: controlRoot.palette.buttonText
    font.pixelSize: Style.size_bodyText
    font.family: Style.string_textFamily
    height: Style.size_bodyText *2
    leftPadding: 10
    rightPadding: 10
    spacing: 10

    palette.buttonText:{
        if(type==="outline"){
            return bgColor
        }else if(type==="flat"){
            return textColor
        }else{
            return textColor
        }
    }

    background: Rectangle {
        implicitWidth: 100
        implicitHeight: 40
        radius: borderRadius
        border.width: 1
        border.color:{
            if(type==="outline"){
                return controlRoot.hovered?(borderColor):"transparent"
            }else if(type==="flat"){
                return "transparent"
            }else{
                return borderColor
            }
        }
        color: {
            if(type==="outline"){
                return "transparent"
            }else if(type==="flat"){
                return controlRoot.hovered?Qt.lighter(bgColor,1.1):bgColor
            }else{
                return controlRoot.hovered?Qt.lighter(bgColor,1.1):bgColor
            }
        }

    }

    opacity: controlRoot.enabled?__opacity:0.6
    onPressed: __opacity = 0.5
    onReleased: __opacity = 1
}
