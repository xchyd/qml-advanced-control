﻿import QtQuick.Controls 2.14
import QtQuick 2.14
import "../style.js" as Style

/*
    公告板控件
*/
Rectangle{
    id:controlRoot

    property string title:""
    property int    titleTextSize:Style.size_titleText
    property color  titleTextColor:"white"
    property string titleTextFamily:Style.string_textFamily
    property int    titleTextAlign:Text.AlignLeft
    property color  titleBgColor:Style.color_flatBlue
    property bool   titleBgUseGradient:false

    readonly property int  availableHeight:contentItem.height
    readonly property int  availableWidth:contentItem.width

    default property list<Item> __items

    border.color:Style.color_borderGray
    border.width: 1
    radius: 8
    width: 300
    height: 300

    //标题
    Rectangle{
        id:titleRect
        y:controlRoot.border.width
        width: parent.width-controlRoot.border.width*2
        height: controlRoot.titleTextSize*2
        anchors.horizontalCenter: parent.horizontalCenter
        radius:controlRoot.radius-2
        gradient: Gradient{
            GradientStop { position: 0.0; color: titleBgColor}
            GradientStop { position: 0.5; color: titleBgUseGradient?Qt.darker(titleBgColor,1.11):titleBgColor}
            GradientStop { position: 1.0; color: titleBgUseGradient?Qt.darker(titleBgColor,1.25):titleBgColor}
        }

        Rectangle{
            width: parent.width
            height: parent.height/2
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            gradient: Gradient{
                GradientStop { position: 0; color: titleBgUseGradient?Qt.darker(titleBgColor,1.11):titleBgColor}
                GradientStop { position: 1.0; color: titleBgUseGradient?Qt.darker(titleBgColor,1.25):titleBgColor}
            }
        }

        Text {
            text:controlRoot.title
            anchors.fill: parent
            horizontalAlignment: controlRoot.titleTextAlign
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: controlRoot.titleTextSize
            font.family: controlRoot.titleTextFamily
            color:controlRoot. titleTextColor
            leftPadding: horizontalAlignment==Text.AlignHCenter?0:10
        }

        Rectangle{
            color: controlRoot.border.color
            width: parent.width
            height: controlRoot.border.width
            anchors.bottom: parent.bottom
        }
    }

    //内容
    Item{
        id: contentItem
        width: parent.width-controlRoot.border.width*2
        height: parent.height-controlRoot.border.width*2-titleRect.height
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: titleRect.bottom
        children: __items
    }
}
