﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "../style.js" as Style

/*
    侧边栏导航控件
*/
Item {
    id:controlRoot

    //导航项配
    property var model:[]
    readonly property string currentSelectedLabel: __currentSelectedLabel

    //一级标题样式
    property int    mainLabelHeight:50
    property color  mainLabelNormalBgColor:"#2F4050"
    property color  mainLabelHoverdBgColor:"#293846"
    property color  mainLabelNormalTextColor:"#A7B1C2"
    property color  mainLabelHoverdTextColor:"white"
    property color  mainLabelSelectedTextColor:"white"
    property bool   mainLabelHoverdTextBold:false
    property int    mainLabelTextSize:20
    property bool   mainLabelShowLeftIcon:true   //是否显示主标题左侧图标
    property bool   mainLabelShowRightIcon:true //是否显示主标题右侧图标
    property url    mainLabelExpandIcon:""      //主标题伸展时右侧显示的图标
    property url    mainLabelCollapseIcon:""    //主标题收缩时右侧显示的图标
    property int    mainLabelCollapseDuration:500  //子标题收缩动画持续时间

    //二级标题样式
    property int    subLabelHeight:40
    property color  subLabelNormalBgColor:"#293846"
    property color  subLabelHoverdBgColor:"#293846"
    property color  subLabelNormalTextColor:"white"
    property color  subLabelHoverdTextColor:"white"
    property color  subLabelSelectedTextColor:"white"
    property bool   subLabelHoverdTextBold:true
    property int    subLabelTextSize:19

    //左侧竖线样式
    property bool   showLeftLine:true
    property int    leftLineWidth:5
    property color  leftLineColor:"#19AA8D"

    //信号
    signal itemClicked(var label);  //点击主标题以及子标题时发出，label指示了被点击的导航项的标题名称

    //内部数据
    property string __currentSelectedLabel:""

    //model改变时重新加载导航项
    onModelChanged:{
        labelModel.clear()
        for(var i =0;i<model.length;i++){
            var data ={"navItemData":"","isCurrentItem":false}
            data.navItemData =JSON.stringify(model[i])
            labelModel.append(data)
        }
    }

    ListModel{
        id:labelModel
    }

    Rectangle{ //背景
        anchors.fill: parent
        color: mainLabelNormalBgColor
    }

    Column{  //导航项列表
        id:navigationColumn

        Repeater{
            model:labelModel
            delegate:Component{ //导航项组件(包含一个主标题和若干子标题)
                id:content

                Item{
                    id:navigationItem
                    property var labelData : JSON.parse(navItemData) //本主标题及子标题的数据
                    property var labelShow : isCurrentItem           //本主标题是否为当前被选中标题
                    property var labelIndex : index                  //本主标题在数据模型中的索引

                    clip: true
                    width: controlRoot.width
                    height: labelShow?mainLabelHeight+subLabelHeight*subLabelList.childrenCount:mainLabelHeight

                    Behavior on height{
                        PropertyAnimation{
                            properties: "height"
                            duration: mainLabelCollapseDuration
                            easing.type:Easing.OutQuart
                        }
                    }
                    Rectangle{    //主标题
                        id:mainLabelBack
                        width: controlRoot.width
                        height: mainLabelHeight
                        color:{
                            if(navigationItem.labelShow||mainLabelMouseArea.containsMouse){
                                return mainLabelHoverdBgColor
                            }else{
                                return mainLabelNormalBgColor
                            }
                        }

                        Rectangle{  //左侧线
                            id:leftLine
                            width: leftLineWidth
                            height: mainLabelBack.height
                            color: {
                                if(navigationItem.labelShow){
                                    return leftLineColor
                                }else{
                                    return "transparent"
                                }
                            }
                        }


                        RowLayout{  //标题内容
                            spacing: 10
                            width: parent.width
                            height: parent.height

                            Button{
                                id:leftIconButton
                                Layout.preferredWidth: mainLabelHeight*0.6
                                Layout.preferredHeight: mainLabelHeight*0.6
                                Layout.leftMargin: 10
                                Layout.alignment: Qt.AlignVCenter
                                display: Button.IconOnly
                                icon.source:labelData.icon===undefined?"":labelData.icon
                                icon.color: mainLabelText.color
                                icon.width:mainLabelHeight*0.6
                                icon.height:mainLabelHeight*0.6
                                background: Rectangle{
                                    color: "transparent"
                                }
                                visible: mainLabelShowLeftIcon
                            }

                            Text {
                                id: mainLabelText
                                text: labelData.main                            
                                Layout.fillWidth: true
                                Layout.fillHeight: true
                                Layout.leftMargin: mainLabelShowLeftIcon?0:20
                                verticalAlignment: Text.AlignVCenter
                                color: {                                    
                                    if(__currentSelectedLabel===mainLabelText.text){
                                        return mainLabelSelectedTextColor;
                                    }else if(navigationItem.labelShow||mainLabelMouseArea.containsMouse){
                                        return mainLabelHoverdTextColor
                                    }else{
                                        return mainLabelNormalTextColor
                                    }
                                }
                                font.pixelSize: mainLabelTextSize
                                font.bold: mainLabelHoverdTextBold? (mainLabelMouseArea.containsMouse?true:false):false
                            }

                            Button{
                                id:rightIconButton
                                Layout.preferredWidth: mainLabelHeight*0.6
                                Layout.preferredHeight: mainLabelHeight*0.6
                                Layout.rightMargin: 10
                                Layout.alignment: Qt.AlignVCenter
                                display: Button.IconOnly
                                icon.source:{
                                    if(navigationItem.labelShow){
                                        return mainLabelExpandIcon
                                    }else{
                                        return mainLabelCollapseIcon
                                    }
                                }
                                icon.color: mainLabelText.color
                                icon.width:mainLabelHeight*0.6
                                icon.height: mainLabelHeight*0.6
                                background: Rectangle{
                                    color: "transparent"
                                }
                                visible: mainLabelShowRightIcon?(navigationItem.labelData.subList.length>0):false
                            }
                        }

                        MouseArea{
                            id:mainLabelMouseArea
                            anchors.fill: parent
                            hoverEnabled: true
                            onClicked: {
                                if(navigationItem.labelShow){  //当前展开则收起
                                    labelModel.setProperty(labelIndex,"isCurrentItem",false)
                                }else{   //当前收起则展开
                                     //先关闭掉所有主标题,再显示本标题
                                    for(var i=0;i<labelModel.count;i++){
                                        labelModel.setProperty(i,"isCurrentItem",false)
                                    }
                                    labelModel.setProperty(labelIndex,"isCurrentItem",true)
                                }

                                //如果本主标题没有子标题，则当前选中的标题就是本标题
                                if(navigationItem.labelData.subList.length===0){
                                    __currentSelectedLabel= mainLabelText.text
                                    itemClicked(__currentSelectedLabel)
                                }
                            }
                        }
                    }//主标题

                    Column{    //子标题
                        id:subLabelList
                        property int childrenCount:subLabelRepeater.model.length
                        width: parent.width
                        height: subLabelHeight*childrenCount
                        clip: true
                        anchors.top:mainLabelBack.bottom

                        Repeater{
                            id:subLabelRepeater
                            model:navigationItem.labelData.subList
                            delegate: Rectangle{
                                id:subLabelBack
                                width: controlRoot.width
                                height: subLabelHeight
                                color: subLabelMousArea.containsMouse?subLabelHoverdBgColor:subLabelNormalBgColor

                                Text{
                                    id:subLabelText
                                    anchors.verticalCenter: parent.verticalCenter
                                    anchors.left: parent.left
                                    anchors.leftMargin:mainLabelText.x+10
                                    text: modelData
                                    color: {
                                        if(__currentSelectedLabel===subLabelText.text){
                                            return subLabelSelectedTextColor;
                                        }else if(navigationItem.labelShow||subLabelMousArea.containsMouse){
                                            return mainLabelHoverdTextColor
                                        }else{
                                            return mainLabelNormalTextColor
                                        }
                                    }
                                    font.pixelSize: subLabelTextSize
                                    font.bold:{
                                       if(!subLabelHoverdTextBold){
                                            return false
                                       }

                                       if(__currentSelectedLabel===subLabelText.text||subLabelMousArea.containsMouse){
                                            return true
                                       }else{
                                            return false
                                       }
                                   }
                                }

                                Rectangle{
                                    width: leftLineWidth
                                    height: navigationItem.height
                                    color: {
                                        if(navigationItem.labelShow){
                                            return leftLineColor
                                        }else{
                                            return "transparent"
                                        }
                                    }
                                    visible: true
                                }

                                MouseArea{
                                    id:subLabelMousArea
                                    hoverEnabled: true
                                    anchors.fill: parent
                                    onClicked: {
                                        var mainLabel = mainLabelText.text
                                        var subLabel = subLabelText.text
                                        __currentSelectedLabel=subLabel
                                        itemClicked(__currentSelectedLabel)
                                    }
                                }
                            }
                        }
                    } //子标题
                }
            }//导航项组件
        }
    }//导航项列表

    function selectedItem(mainLabel,subLabel){
        for(var i=0;i<labelModel.count;i++){
            var labelData= labelModel.get(i)
            var labelValue = JSON.parse(labelData.value)
            if(labelValue.main===mainLabel){
                labelModel.setProperty(i,"isShow",true)
                break;
            }
        }

        if(subLabel===""){
            __currentSelectedLabel= mainLabel
        }else{
            __currentSelectedLabel= subLabel
        }
        itemClicked(__currentSelectedLabel)

//        for (var i2 =0;i2<labelModel.count;i2++){
//            console.log("\nSideBar Modle:\n",JSON.stringify(labelModel.get(i2)),"\n")
//        }
    }

}
