﻿import QtQuick.Controls 2.14
import QtQuick.Layouts 1.12
import QtQuick 2.12
import QtGraphicalEffects 1.14
import "../style.js" as Style

/*
    繁忙指示器控件
*/
BusyIndicator {
    id: controlRoot

    property int    rotationSpeed:1100              //旋转速度（选择一圈耗时ms）
    property color  headColor:Style.color_flatBlue  //旋转圈的头部颜色（建议使用深色）
    property color  tailColor:"white"               //选择圈的尾部颜色（建议使用浅色）

    implicitWidth: 50
    implicitHeight: 50

    contentItem: Item {
        Rectangle {
            id: rect
            width: parent.width
            height: parent.height
            color: Qt.rgba(0, 0, 0, 0)
            radius: width / 2
            border.width: width / 6
            visible: false
        }

        ConicalGradient {
            width: rect.width
            height: rect.height
            gradient: Gradient {
                GradientStop { position: 0.0; color: tailColor }
                GradientStop { position: 1.0; color: headColor }
            }
            source: rect

            Rectangle {
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                width: rect.border.width
                height: width
                radius: width / 2
                color: headColor
            }

            RotationAnimation on rotation {
                running: controlRoot.visible
                from: 0
                to: 360
                duration: controlRoot.rotationSpeed
                loops: Animation.Infinite
            }
        }
    }

}
