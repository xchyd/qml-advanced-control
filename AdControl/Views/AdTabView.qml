﻿import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12 as Control2
import QtQuick.Controls 1.4 as Control1
import QtQuick.Controls.Styles 1.4
import "../style.js" as Style

/*
    选项卡控件
*/
Control1.TabView {
    id: controlRoot

    property string type:"card"                     //Tab按钮类型：plain（下划线风格）、card（选项卡风格）

    property color  frameBgColor:"white"
    property color  frameBorderColor:Style.color_borderGray

    property int    tabBarHeight:tabTextSize*2
    property color  tabBarBgColor: Qt.lighter(Style.color_borderGray,1.25)
    property color  tabBarBorderColor: frameBorderColor

    property string tabTextFamily:Style.string_textFamily
    property int    tabTextSize:Style.size_bodyText
    property color  tabTextNormalColor:Style.color_textBlack
    property color  tabTextSelectedColor:Style.color_flatBlue
    property color  tabBgNormalColor:tabBarBgColor
    property color  tabBgSelectedColor:frameBgColor
    property color  tabBgHoveredColor:Qt.darker(tabBarBgColor,1.25)

    property color  tabBottomLineColor:Style.color_flatBlue      //Tab按钮底部下划线颜色（仅在plain样式有效）
    property bool   tabBottomDividerVisible:false                //是否显示TabBar与View间的分割线（仅在plain样式有效）
    property color  tabBottomDividerColor:Style.color_borderGray //TabBar与View间的分割线的颜色（仅在plain样式有效）

    property bool   tabCloseButtonVisible:true
    property bool   tabMovable:true
    property int    tabAlignment: Qt.AlignLeft

    width: 400
    height: 300

    style: type==="plain"? plainStyleComp:cardStyleComp


    //朴素风格样式（下划线风格样式）
    Component{
        id:plainStyleComp

        TabViewStyle {
            tabOverlap: 1
            frameOverlap:1
            tabsMovable:tabMovable
            tabsAlignment: tabAlignment

            //Tab按钮样式
            tab: Rectangle{
                color: "transparent"
                implicitWidth: text.contentWidth+50
                implicitHeight: tabBarHeight

                RowLayout{
                    clip: true
                    anchors.centerIn: parent
                    spacing: 20

                    //标签文本
                    Text {
                        id: text
                        text: styleData.title
                        color: styleData.selected ? tabTextSelectedColor:tabTextNormalColor
                        font.pixelSize: tabTextSize
                    }

                    //关闭按钮
                    Rectangle{
                        visible: tabCloseButtonVisible
                        height: text.height
                        width: height
                        radius: width
                        color: mouseArea.containsMouse?Qt.darker(tabBarBgColor,1.25):"transparent"

                        Text{
                            id:closeText
                            text: "×"
                            color: mouseArea.containsMouse?"white":tabTextNormalColor
                            font.bold: mouseArea.containsMouse
                            font.pixelSize: tabTextSize
                            anchors.centerIn: parent
                        }

                        MouseArea{
                            id:mouseArea
                            hoverEnabled: true
                            anchors.fill: parent
                            onClicked: {
                                controlRoot.removeTab(styleData.index)
                            }
                        }
                    }
                }

                Rectangle{
                    color: tabBottomLineColor
                    height: 2
                    width: parent.width-2
                    anchors.bottom: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    visible: styleData.selected
                }
            }

            //Tab按钮组背景样式
            tabBar:Rectangle{
                color: tabBarBgColor
                border.color: frameBorderColor

                Rectangle{
                    width: parent.width
                    height: 1
                    visible: tabBottomDividerVisible
                    color: tabBottomDividerColor
                    anchors.bottom: parent.bottom
                }
            }

            //View背景样式
            frame: Rectangle {
                color: frameBgColor
                border.color: frameBorderColor
                border.width: 1
            }
        }
    }


    //选项卡风格样式
    Component{
        id:cardStyleComp

        TabViewStyle {
            tabOverlap: 1   // Tab按钮的间距
            frameOverlap:1  // Tab按钮底端到边框的距离，大于0将产生重合，小于0将分离
            tabsMovable:tabMovable
            tabsAlignment: tabAlignment

            //Tab按钮样式
            tab: Item{
                implicitWidth: text.contentWidth+60
                implicitHeight: tabBarHeight

                Rectangle {
                    width: parent.width-2
                    height: parent.height-2
                    color: styleData.selected ? tabBgSelectedColor :(styleData.hovered?tabBgHoveredColor:tabBgNormalColor)
                    anchors.centerIn: parent

                    RowLayout{
                        clip: true
                        anchors.centerIn: parent
                        spacing: 20

                        //标签文本
                        Text {
                            id: text
                            text: styleData.title
                            color: styleData.selected ? tabTextSelectedColor:tabTextNormalColor
                            font.pixelSize: tabTextSize
                            font.family: tabTextFamily
                        }

                        //关闭按钮
                        Rectangle{
                            visible: tabCloseButtonVisible?styleData.hovered:false
                            height: text.height
                            width: height
                            radius: width
                            color: mouseArea.containsMouse?Qt.darker(tabBarBgColor,1.25):"transparent"

                            Text{
                                id:closeText
                                text: "×"
                                color: mouseArea.containsMouse?"white":tabTextNormalColor
                                font.bold: mouseArea.containsMouse
                                font.pixelSize: tabTextSize
                                anchors.centerIn: parent
                            }

                            MouseArea{
                                id:mouseArea
                                hoverEnabled: true
                                anchors.fill: parent
                                onClicked: {
                                    controlRoot.removeTab(styleData.index)
                                }
                            }
                        }
                    }


                    Rectangle{
                        color:frameBgColor
                        height: 1
                        width: parent.width-2
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.bottom: parent.bottom
                        visible: styleData.selected
                    }
                }
            }

            //Tab按钮组背景样式
            tabBar:Rectangle{
                color: tabBarBgColor
                border.color: tabBarBorderColor         
            }

            //View背景样式
            frame: Rectangle {
                color: frameBgColor
                border.color: frameBorderColor
                border.width: 1
            }
        }
    }
}
