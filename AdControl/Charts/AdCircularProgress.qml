﻿import QtQuick 2.12
import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import "../style.js" as Style

/*
    进度环控件
*/
Item{
    id:controlRoot

    //基本设置
    property real   value:0               //当前进度值(0~1)

    //文本样式
    property bool   textVisible:true      //是否显示进度文本
    property int    textSize:(canvas.width/2-bgRingWidth)*0.5
    property color  textColor:Style.color_textBlack
    property string textFamily:Style.string_textFamily

    //圆环样式
    property int      spacing:0                   //进度环与背景环间距
    property int      bgRingWidth:15              //背景环宽度
    property color    bgRingColor:Qt.lighter(Style.color_borderGray,1.25)      //背景环颜色
    property color    progressRingColor:Style.color_flatBlue //进度环颜色
    property color    bgColor:"white"             //环中央空洞的背景颜色

    //内部数据
    width: 200
    height: 200
    onValueChanged: {
        canvas.requestPaint()
    }

    Canvas{
        id: canvas
        height: Math.ceil(controlRoot.width,controlRoot.height)
        width: canvas.height
        anchors.centerIn: parent

        Text{
            anchors.centerIn: parent
            font.pixelSize:textSize
            font.family: textFamily
            color:textColor
            text: value*100+"%"
        }

        onPaint: {
            var ctx = getContext("2d")
            ctx.clearRect(0,0,width,height)

            //绘制背景色
            ctx.beginPath()
            ctx.fillStyle = bgColor
            ctx.lineWidth = 1
            ctx.arc(canvas.width/2,canvas.height/2,canvas.width/2-bgRingWidth ,0,Math.PI*2,false)
            ctx.fill()

            //绘制背景环
            ctx.beginPath()
            ctx.strokeStyle = bgRingColor
            ctx.lineWidth = bgRingWidth
            ctx.arc(canvas.width/2,canvas.height/2,canvas.width/2-bgRingWidth ,0,Math.PI*2,false)
            ctx.stroke()

            //绘制进度环
            var r = value*2*Math.PI
            ctx.beginPath()
            ctx.strokeStyle = progressRingColor
            ctx.lineWidth = bgRingWidth-2*spacing
            ctx.arc(canvas.width/2,canvas.height/2,canvas.width/2-bgRingWidth, -Math.PI/2,-Math.PI/2+r,false)
            ctx.stroke()

        }
    }
}
